import services.daysOfCode.*
import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {
    val currentDay = DayOfCode25()
    println("Start of program day ${currentDay.dayNumber}")

    val timeInMillisPart1 = measureTimeMillis {
        println("Answer 1: ${currentDay.getAnswer(1)}")
    }
    println("Done in: ${timeInMillisPart1}ms")

    val timeInMillisPart2 = measureTimeMillis {
        println("Answer 2: ${currentDay.getAnswer(2)}")

    }
    println("Done in: ${timeInMillisPart2}ms")


}