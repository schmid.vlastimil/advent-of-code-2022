package services.daysOfCode

/**
 *                                                          ___
 *                                                    ,----'   `-,
 *                           ___,-'~~~`---'~~~~`-----' ,-'        \,
 *                   ___,---'          '        ,-~~~~~            :
 *              _,--'                 ,        ; ;       ) "   __   \,
 *         _,--'     ,                 ,'      :: "  ;  ` ,'  (\o\)  |
 *        / _,       `,                     ,  `; " ;    (     `~~ `'\
 *       ; /         ,`               ,     `    ~~~`. " ;   _     ,  `.
 *      ,'/          ` ,              `     ` ,  ,    \_/ ?   ;    )   `.
 *      :;:            `                      `  ` ,     uu`--(___(    ~:
 *      :::          , ,  ,            ,   ;     , `  ,-'      \~(  ~   :
 *      ||:          ` `  `         ,  ` ,'    ( ` _,-          \ \_   ~:
 *      :|`.        , ,  ,          `_   ;       ) );            \__>   :
 *      |:  : ;     ` `  ` ;  __,--~~ `-(         ( |              `.  ~|
 *      :|  :         ` __,--'           :  ()    : :               |~  ;
 *      |;  |  `     ,-'    ;             :  )(   | `.         /(   `. ~|
 *      ::  :   ~  _/;     ;               |   )  :  :        ; /    ;~ ;
 *      {}  ;     /  :   ~ :               :      ; ,;        : `._,-  ,
 *      {} /~    /   ;    ;                 : ,  |  `;         `.___,-'
 *        ;~    ;    ;  ~ `.                | `  )   ;
 *        :`    \    `;~   \                ;~   `-, `-.     Targon
 *        `.__OOO;    `;_OOO;               )_____OO;_(O)
 *         ~~~~~~       ~~~~                ~~~~~~~~ ~~~~
 *
 *         Answer 1: 2080
 *         Done in: 313ms
 *         Answer 2: 2752
 *         Done in: 129316ms
 */
class DayOfCode16: ADayOfCode() {
    override var dayNumber = 16
    private lateinit var valves: MutableMap<String, Valve>
    private var bestTotalPressure = 0
    private var isPartTwo = false

    /**
     * 1. parse data to Data Class Valve (valves are in list)
     * 2. get valves that have non zero pressure flow (for better performance)
     * 3. paths for each valves are added with initPathFinding() with depth? first search
     * 4. best pressure search
     */
    override fun makePartOne(): String {
        valves = parseValves()
        val nonZeroKeys = getNonZeroValveNames(valves)
        initPathFinding(nonZeroKeys.toList(), "AA")
        findBestTotalPressure(0, "AA", 29, setOf("AA"))

        return bestTotalPressure.toString()
    }

    /**
     * Everything is the same, only elephant is added
     * elephant is ignored, but after first open valve elephant is activated
     * Time exec: ~160s
     */
    override fun makePartTwo(): String {
        // reset pressure
        bestTotalPressure = 0
        isPartTwo = true
        findBestTotalPressure(0, "AA", 25, setOf("AA"))

        return bestTotalPressure.toString()
    }



    fun findBestTotalPressure( totalPressure: Int, currentValveName: String, remainingTime: Int, openedValves: Set<String>, isTurnOfElephant: Boolean = false) {
        if (totalPressure > bestTotalPressure) bestTotalPressure = totalPressure
        if (remainingTime <= 0) return

        if (!openedValves.contains(currentValveName)) {
            val pressureFlowed = totalPressure + valves[currentValveName]!!.flow * remainingTime
            findBestTotalPressure(pressureFlowed, currentValveName, remainingTime - 1, openedValves.union(mutableListOf(currentValveName)), isTurnOfElephant)
            if (!isTurnOfElephant && isPartTwo) {
                // Split in two after opened valve
                findBestTotalPressure(pressureFlowed, "AA", 25, setOf(currentValveName).union(openedValves), true)
            }
        } else {
            val keysOfPaths = valves[currentValveName]!!.paths.keys
            val subtracted = keysOfPaths.subtract(openedValves)
            subtracted.forEach { valveName ->
                findBestTotalPressure(totalPressure, valveName, remainingTime - valves[currentValveName]!!.paths[valveName]!!, openedValves, isTurnOfElephant)
            }
        }
    }


    fun algorithm(valveNames: List<String>, finalValve: String): Int {
        var currentValveNames = valveNames
        var depth = 1
        while (true) {
            val nextValveNames = MutableList(0) {"AA"}
            currentValveNames.forEach {tunnelCurrent ->
                if (tunnelCurrent == finalValve) {
                    return depth
                } else {
                    valves[tunnelCurrent]?.tunnels?.forEach { tunnelNext ->
                        nextValveNames.add(tunnelNext)
                    }
                }
            }
            currentValveNames = nextValveNames
            depth++
        }
    }

    /**
     * nonZeroValveNames + AA
     */
    fun initPathFinding(nonZeroValveNames: List<String>, startValve: String) {
        val nonZeroWithStart = nonZeroValveNames.toMutableList()
        nonZeroWithStart.add(startValve)
        nonZeroWithStart.forEach { wStart ->
            nonZeroValveNames.forEach { nonStart ->
                if (wStart != nonStart) {
                    // i dont get this part auto generated by ide xd
                    valves[wStart]?.let { algorithm(it.tunnels, nonStart) }
                        ?.let { valves[wStart]?.paths?.set(nonStart, it) }
                }
            }
        }
    }

    fun getNonZeroValveNames(valves: MutableMap<String, Valve>): List<String> {
        val names = MutableList(0) {"AA"}
        valves.forEach { valve ->
            if (valve.value.flow != 0) {
                names.add(valve.key)
            }
        }
        return names.toList()
    }

    fun parseValves(): MutableMap<String, Valve> {
        var inputData = getInputData(1)
        inputData = inputData.replace("Valve ", "")
            .replace("has flow rate=", "")
            .replace(",", "")
            .replace(";", "")
            .replace("tunnels ", "")
            .replace("tunnel ", "")
            .replace(" valves", "")
            .replace(" valve", "")
            .replace("lead ", "")
            .replace("leads ", "")

        var valves = emptyMap<String, Valve>()
        valves = valves.toMutableMap()

        inputData.split("\n").forEach { line ->
            val currentValveArr = line.split("to")[0].split(" ")
            val tunnels = line.split("to")[1].split(" ")
            val currentValve = currentValveArr[0]
            val currentFlow = currentValveArr[1].toInt()
            val mutTunnels = MutableList(0) { "AA" }
            tunnels.forEach { tunnel ->
                if (tunnel != "") {
                    mutTunnels.add(tunnel)
                }
            }
            val mutMapPaths = emptyMap<String, Int>().toMutableMap()
            valves[currentValve] = Valve(currentValve, currentFlow, mutTunnels.toList(), mutMapPaths)

        }
        return valves
    }

    data class Valve(
        var name: String,
        var flow: Int,
        var tunnels: List<String>,
        var paths: MutableMap<String, Int>
    ) {

    }
}

