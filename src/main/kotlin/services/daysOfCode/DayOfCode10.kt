package services.daysOfCode

class DayOfCode10: ADayOfCode() {
    override var dayNumber = 10

    override fun makePartOne(): String {
        val inputData = getInputData(2)

        var signalStrengthSum = 0
        var registerX = 1
        var cycle = 0
        inputData.split("\n").forEach {command ->
            var cycleIncrement = 0
            val commandSplit = command.split(" ")
            if (commandSplit[0] == "addx") {
                cycleIncrement = 2
            } else if (commandSplit[0] == "noop") {
                cycleIncrement = 1
            }

            for (cycleInc in 1 .. cycleIncrement) {
                cycle++
                if (
                    cycle == 20  ||
                    cycle == 60  ||
                    cycle == 100 ||
                    cycle == 140 ||
                    cycle == 180 ||
                    cycle == 220
                ) {
                    signalStrengthSum += cycle * registerX
                }
            }

            if (commandSplit[0] == "addx") {
                registerX += commandSplit[1].toInt()
            }
        }

        return signalStrengthSum.toString()
    }

    override fun makePartTwo(): String {
        val inputData = getInputData(1)

        var signalStrengthSum = 0
        var registerX = 1
        var cycle = 0
        inputData.split("\n").forEachIndexed {index, command ->
            var cycleIncrement = 0
            val commandSplit = command.split(" ")
            if (commandSplit[0] == "addx") {
                cycleIncrement = 2
            } else if (commandSplit[0] == "noop") {
                cycleIncrement = 1
            }

            for (cycleInc in 1 .. cycleIncrement) {
                if (
                    registerX == cycle ||
                    registerX == cycle + 1 ||
                    registerX == cycle - 1
                ) {
                    print("#")
                } else {
                    print(".")
                }
                cycle++

                if (cycle == 40) {
                    cycle = 0
                    println()
                }
            }

            if (commandSplit[0] == "addx") {
                registerX += commandSplit[1].toInt()
            }

        }

        return signalStrengthSum.toString()
    }
}

