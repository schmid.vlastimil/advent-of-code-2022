package services.daysOfCode

import java.util.LinkedList

//      ,*-'.
//      .'+*
//    '   #   '
//       /v\
//     ,'  .`.
//__.-"  ^    "-.__
//             cgmm
class DayOfCode18: ADayOfCode() {
    override var dayNumber = 18

    /**
     * Answer 1: 4390
     * Done in: 23ms
     */
    override fun makePartOne(): String {
        makeDirections()
        val points = makePoints()
        val map = makeMap(points)
        return getSurfaceArea(points, map).toString()
    }

    /**
     * Answer 1: 2534
     * Done in: 20ms
     * I needed to repurpose recursive function with flood algorithm to Queue based ... kotlin did not like that big
     * recursion
     * 1. get normal surface area
     * 2. get air pocket point with flooding those point that are not "inside"
     * 3. inverse air pockets
     * 4. calculate answer
     */
    override fun makePartTwo(): String {
        val points = makePoints()
        val map = makeMap(points)

        val surfaceArea = getSurfaceArea(points, map)
        val airPocketPoints = getAirPocketPoints(points)
        inverseMap(map)
        val airPocketSurfaceArea = getSurfaceArea(airPocketPoints, map)
        val insideArea = surfaceArea - airPocketSurfaceArea
        return insideArea.toString()
    }

    // debug vars
    val pointsOffset = 0
    val mapOffset = 1

    fun makePoints(): MutableList<Point3D> {
        val inputData = getInputData(1)

        val points = MutableList(0) { Point3D(0,0,0) }
        inputData.split("\n").forEach { line ->
            val splitXYZ = line.split(',').map { it.toInt() }
            points.add(Point3D(splitXYZ[0]+pointsOffset, splitXYZ[1]+pointsOffset, splitXYZ[2]+pointsOffset))
        }
        return points
    }

    fun inverseMap(map: MutableList<MutableList<MutableList<Boolean>>>) {
        map.forEachIndexed { x, mapX ->
            mapX.forEachIndexed { y, mapY ->
                mapY.forEachIndexed { z, mapZ ->
                    map[x][y][z] = !mapZ
                }
            }
        }
    }

    fun getAirPocketPoints(points: MutableList<Point3D>): MutableList<Point3D> {
        val mapToFlood = makeMap(points)

        val maxX = points.getMaxX()
        val maxY = points.getMaxY()
        val maxZ = points.getMaxZ()

        mapToFlood.forEachIndexed { x, mapX ->
            mapX.forEachIndexed { y, mapY ->
                mapY.forEachIndexed { z, _ ->
                    floodMapFrom(mapToFlood, Point3D(x, y,0))
                    floodMapFrom(mapToFlood, Point3D(x,0,z))
                    floodMapFrom(mapToFlood, Point3D(0,y,z))

                    floodMapFrom(mapToFlood, Point3D(x, y, maxZ))
                    floodMapFrom(mapToFlood, Point3D(maxX, y, z))
                    floodMapFrom(mapToFlood, Point3D(x, maxY, z))
                }
            }
        }

        val airPocketPoints = MutableList(0) { Point3D(0,0,0) }

        mapToFlood.forEachIndexed { x, mapX ->
            mapX.forEachIndexed { y, mapY ->
                mapY.forEachIndexed { z, mapZ ->
                    if (!mapZ) {
                        airPocketPoints.add(Point3D(x,y,z))
                    }
                }
            }
        }

        return airPocketPoints
    }

    fun floodMapFrom(map: MutableList<MutableList<MutableList<Boolean>>>, point: Point3D) {
        val queue = LinkedList<Point3D>()
        queue.push(point)
        while (queue.isNotEmpty()) {
            val pointFromQueue = queue.removeFirst()
            var add = true
            if (!map.exists(pointFromQueue)) {
                add = false
            }
            if (add && !map[pointFromQueue.x][pointFromQueue.y][pointFromQueue.z]) {
                map[pointFromQueue.x][pointFromQueue.y][pointFromQueue.z] = true
            } else {
                add = false
            }
            if (add) {
                directions.forEach {  direction ->
                    val newPoint = pointFromQueue.getTranslated(direction)
                    queue.push(newPoint)
                }
            }

        }
    }


    fun getSurfaceArea(points: MutableList<Point3D>, map: MutableList<MutableList<MutableList<Boolean>>>): Int {

        var sidesCount = 0
        points.forEach { point ->
            directions.forEach { direction ->
                val newPoint = point.getTranslated(direction)
                if ( map.exists(newPoint) && !map[newPoint.x][newPoint.y][newPoint.z] ) {
                    sidesCount++
                }
                if (!map.exists(newPoint)) {
                    sidesCount++
                }
            }
        }
        return sidesCount
    }


    fun makeMap(points: MutableList<Point3D>): MutableList<MutableList<MutableList<Boolean>>> {
        val map = makeEmptyMap(points)
        points.forEach {
            map[it.x][it.y][it.z] = true
        }
        return map
    }

    fun makeEmptyMap(points: MutableList<Point3D>): MutableList<MutableList<MutableList<Boolean>>> {

        val maxX = points.getMaxX() + mapOffset
        val maxY = points.getMaxY() + mapOffset
        val maxZ = points.getMaxZ() + mapOffset

        return MutableList(maxX) { MutableList(maxY) { MutableList(maxZ) { false } } }
    }

    val directions = MutableList(0) { Point3D(0,0,0) }
    fun makeDirections(): MutableList<Point3D> {
        directions.add(Point3D(0,0,1))
        directions.add(Point3D(0,0,-1))
        directions.add(Point3D(0,1,0))
        directions.add(Point3D(0,-1,0))
        directions.add(Point3D(1,0,0))
        directions.add(Point3D(-1,0,0))
        return directions
    }

    class Point3D(var x: Int, var y: Int, var z: Int) {
        constructor(point3D: Point3D) : this(point3D.x, point3D.y, point3D.z)

        fun getTranslated(point: Point3D): Point3D {
            val newPoint = Point3D(this.x + point.x, this.y + point.y, this.z + point.z)
            return newPoint
        }
    }
    private fun MutableList<Point3D>.getMaxX(): Int {
        return this.maxOf { it.x }
    }
    private fun MutableList<Point3D>.getMaxY(): Int {
        return this.maxOf { it.y }
    }
    private fun MutableList<Point3D>.getMaxZ(): Int {
        return this.maxOf { it.z }
    }
    private fun MutableList<MutableList<MutableList<Boolean>>>.exists(point: Point3D): Boolean {
        return !(this.getOrNull(point.x) == null ||
                this[point.x].getOrNull(point.y) == null ||
                this[point.x][point.x].getOrNull(point.z) == null)
    }
}



