package services.daysOfCode

class DayOfCode03: ADayOfCode() {
    override var dayNumber = 3

    override fun makePartOne(): String {
        val inputData = getInputData(1);
        var prioritiesSum = 0;
        inputData.split("\n")
            .forEach { characters ->
                val countOfChars = characters.count()
                val firstSection = characters.subSequence(0, countOfChars / 2).toSet()
                val secondSection = characters.subSequence(countOfChars / 2, countOfChars).toSet()

                prioritiesSum += firstSection.intersect(secondSection)
                    .sumOf { getValueOfChar(it) }
            }

        return prioritiesSum.toString();
    }

    override fun makePartTwo(): String {
        val inputData = getInputData(1);

        return inputData.split("\n")
            .windowed(3, 3)
            .map { it.map { backpackItems -> backpackItems.toSet() } }
            .map { backpack -> backpack.reduce {acc, items -> acc.intersect(items)} }
            .sumOf { getValueOfChar(it.first()) }
            .toString()
    }

    fun getValueOfChar(char: Char): Int {
        if (char.isUpperCase()) {
            return char.toInt() - 38
        }

        return char.toInt() - 96
    }
}

