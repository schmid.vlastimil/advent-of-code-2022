package services.daysOfCode

import java.util.Collections
import kotlin.math.abs
import kotlin.math.sign

/*
 *Answer 1: 8028
 *Done in: 26ms
 *Answer 2: 8798438007673
 *Done in: 127ms
 */
class DayOfCode20: ADayOfCode() {
    override var dayNumber = 20

    override fun makePartOne(): String {
        return getCoordinateAnswer().toString()
    }

    override fun makePartTwo(): String {
        return getCoordinateAnswer(811589153, 10).toString()
    }

    fun getCoordinateAnswer(decryptKey: Long = 1, numberOfScramble: Int = 1): Long {
        val numbers = parseData(decryptKey)
        val tempNumbers = numbers.toMutableList()

        repeat(numberOfScramble) {
            numbers.forEach { numberOrig ->
                val indexOf = tempNumbers.indexOf(numberOrig)
                val removed = tempNumbers.removeAt(indexOf)
                val indexTo = (numberOrig.second + indexOf).mod(tempNumbers.size)
                tempNumbers.add(indexTo, numberOrig)
            }
        }

        var sortedNumbers = mutableListOf<Long>()
        tempNumbers.forEach {
            sortedNumbers.add(it.second)
        }


        val indexOfZero = sortedNumbers.indexOf(0)
        var answer:Long = 0
        answer += sortedNumbers[(1000+indexOfZero) % sortedNumbers.size]
        answer += sortedNumbers[(2000+indexOfZero) % sortedNumbers.size]
        answer += sortedNumbers[(3000+indexOfZero) % sortedNumbers.size]

        return answer
    }

    fun parseData(decryptKey: Long = 1): MutableList<Pair<Int, Long>> {
        // Index, number... shift value
        val numbers = mutableListOf<Pair<Int, Long>>()
        getInputData(1).split("\n").forEachIndexed { index, i ->
            numbers.add(Pair(index,i.toLong()*decryptKey))
        }
        return numbers
    }


    var isDebug = false
    fun debugPrint(string: String) {
        if (isDebug)
            print(string)
    }


    private fun  MutableList<Pair<Int, Boolean>>.shiftIndexBy(numberIndex: Int, shiftBy: Int) {
        var numgggg = this[numberIndex]

        if (shiftBy == 0) {
            return
        }

        var reducedShiftBy = shiftBy % this.count()
        var toIndex = 0

        toIndex = numberIndex + reducedShiftBy

        // When overflow
        if (toIndex == 0) {
            toIndex = toIndex + count() - 1
        }
        if (toIndex == count()) {
            toIndex = 0
        }

        if (toIndex < 0) {
            toIndex = toIndex + count() - 1
        } else if (toIndex > count()) {
            toIndex = toIndex - count() + 1
        }



        val removed = this.removeAt(numberIndex)
        this.add(toIndex, removed)

    }

}



