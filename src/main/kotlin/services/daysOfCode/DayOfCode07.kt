package services.daysOfCode

import java.util.TreeMap

class DayOfCode07: ADayOfCode() {
    override var dayNumber = 7

    // 1069240 too low
    override fun makePartOne(): String {
        val dirs = getAllDirs()
        val dirsSummed = sumDirs(dirs)

        var answer = 0
        dirsSummed.forEach {
            if (it.value <= 100000) {
                answer += it.value
            }
        }

        return answer.toString()
    }

    override fun makePartTwo(): String {
        val dirs = getAllDirs()
        val dirsSummed = sumDirs(dirs)

        var answer2 = 70000000

        val maxUsed = 70000000 - 30000000
        var totalUsed = 0
        dirs.forEach {
            totalUsed += it.value
        }
        val needToFree = totalUsed - maxUsed
        dirsSummed.forEach {
            if (it.value >= needToFree && answer2 > it.value) {
                answer2 = it.value
            }
        }

        return answer2.toString()
    }

    fun sumDirs(dirs: TreeMap<String, Int>): TreeMap<String, Int> {
        val dirsSummed = TreeMap<String, Int>()

        dirs.onEachIndexed { mainIndex, main ->
            dirs.onEachIndexed { comIndex, compare ->
                if (compare.key.startsWith(main.key)) {
                    dirsSummed[main.key] = ((dirsSummed[main.key] ?: 0) + compare.value)
                }
            }
        }
        return dirsSummed
    }

    fun getAllDirs(): TreeMap<String, Int> {
        val inputData = getInputData(1)
        val dirs = TreeMap<String, Int>()
        var currentPath = "/"
        inputData.split('\n')
            .forEach {
                if (it == "\$ cd /") {
                    currentPath = "/"
                } else if (it == "\$ ls") {

                } else if (it.split(' ')[0] == "dir") {

                } else if (it.split(' ')[1] == "cd") {
                    if (it.split(' ')[2] == "..") {
                        // Go back
                        val split = currentPath.split('/').toMutableList()
                        split.removeFirst()
                        split.removeLast()
                        split.removeLast()
                        currentPath = "/"
                        split.forEach { dirCurr ->
                            currentPath += dirCurr
                            currentPath += "/"
                        }
                    } else {
                        currentPath += it.split(' ')[2]
                        currentPath += "/"
                    }
                    dirs[currentPath] = (dirs[currentPath] ?: 0)

                } else {
                    // FILES
                    dirs[currentPath] = ((dirs[currentPath] ?: 0) + it.split(' ')[0].toInt())
                }
            }
        return dirs
    }
}

