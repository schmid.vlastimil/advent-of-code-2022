package services.daysOfCode

class DayOfCode06: ADayOfCode() {
    override var dayNumber = 6

    override fun makePartOne(): String {
        return getStartOfPacketMarker(4, getInputData(1))
    }

    override fun makePartTwo(): String {
        return getStartOfPacketMarker(14, getInputData(1))
    }

    fun getStartOfPacketMarker(sequence: Int, buffer: String): String {
        buffer.forEachIndexed { index, _ ->
            val fourMostRecent = buffer.substring(index, index + sequence)
            if (fourMostRecent.allUnique())
                return (index + sequence).toString()
        }
        return "Not Found"
    }

    fun String.allUnique(): Boolean = all(hashSetOf<Char>()::add)
}