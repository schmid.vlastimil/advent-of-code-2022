package services.daysOfCode

class DayOfCode08: ADayOfCode() {
    override var dayNumber = 8

    override fun makePartOne(): String {
        val inputData = getInputData(1)
            .split('\n')

        val rows = inputData.size
        val cols = inputData.first().count()
        val arrayOfTrees = Array(rows) { arrayOfNulls<Int>(cols) }
        val arrayOfTreesCounted = Array(rows) { arrayOfNulls<Int>(cols) }

        inputData.forEachIndexed { indexRow, row ->
            row.forEachIndexed { indexCol, col ->
                arrayOfTrees[indexRow][indexCol] = col.toString().toInt()
            }
        }

        //
        for (direction in 0 until 4) {
            var rangeRow: IntProgression = 0 until rows
            var rangeCol: IntProgression = 0 until cols

            when (direction) {
                0 -> {
                    // from left
                    rangeRow = 0 until rows
                    rangeCol = 0 until cols
                }
                1 -> {
                    // from right
                    rangeRow = 0 until rows
                    rangeCol = (cols - 1) downTo  0
                }
                2 -> {
                    // from top
                    rangeRow = 0 until cols
                    rangeCol = 0 until cols
                }
                3 -> {
                    // from top
                    rangeRow = 0 until cols
                    rangeCol = 0 until cols
                    arrayOfTrees.reverse()
                    arrayOfTreesCounted.reverse()
                }
            }
            for (rowIndex in rangeRow) {
                var highestTree = -1
                for (colIndex in rangeCol) {
                    if (direction == 2 || direction == 3) {
                        val colValue = arrayOfTrees[colIndex][rowIndex]
                        if (colValue != null) {
                            if (colValue > highestTree) {
                                highestTree = colValue
                                arrayOfTreesCounted[colIndex][rowIndex] = colValue
                            }
                        }
                    } else {
                        val colValue = arrayOfTrees[rowIndex][colIndex]
                        if (colValue != null) {
                            if (colValue > highestTree) {
                                highestTree = colValue
                                arrayOfTreesCounted[rowIndex][colIndex] = colValue
                            }
                        }
                    }

                }
            }
        }
        arrayOfTreesCounted.reverse()

        var answer = 0
        arrayOfTreesCounted.forEach {
            it.forEach { value ->
                if (value == null) {
//                    print(" ")
                } else {
                    answer++
//                    print(value)
                }

            }
//            println()
        }


        return answer.toString()
    }

    override fun makePartTwo(): String {
        val inputData = getInputData(1)
            .split('\n')

        val rows = inputData.size
        val cols = inputData.first().count()
        val arrayOfTrees = Array(rows) { arrayOfNulls<Int>(cols) }
        val arrayOfTreesCounted = Array(rows) { arrayOfNulls<Int>(cols) }

        inputData.forEachIndexed { indexRow, row ->
            row.forEachIndexed { indexCol, col ->
                arrayOfTrees[indexRow][indexCol] = col.toString().toInt()
            }
        }

        var highestScore = 0
        arrayOfTrees.forEachIndexed { rowIndex, row ->
            row.forEachIndexed { colIndex, col ->
                val currentTreeHeight = col
                // ---------
                // To RIGHT
                var indexToRight = 0
                var toRightPoints = 0
                while (true) {
                    indexToRight++
                    if (currentTreeHeight != null) {
                        if (arrayOfTrees.getOrNull(rowIndex)  == null || arrayOfTrees.getOrNull(rowIndex)!!.getOrNull(colIndex+indexToRight) == null) {
                            break
                        }
                        var heightOfTreeToDirection = arrayOfTrees[rowIndex][colIndex+indexToRight]!!
                        if ((currentTreeHeight <= heightOfTreeToDirection)) {
                            // END when heigheter tree
                            // and add some points
                            toRightPoints++
                            break
                        } else {
                            toRightPoints++
                        }
                    }
                }
                // To LEFT
                var indexToLeft = 0
                var toLeftPoints = 0
                while (true) {
                    indexToLeft++
                    if (currentTreeHeight != null) {
                        if (arrayOfTrees.getOrNull(rowIndex)  == null || arrayOfTrees.getOrNull(rowIndex)!!.getOrNull(colIndex-indexToLeft) == null) {
                            break
                        }
                        var heightOfTreeToDirection = arrayOfTrees[rowIndex][colIndex-indexToLeft]!!
                        if ((currentTreeHeight <= heightOfTreeToDirection)) {
                            // END when heigheter tree
                            // and add some points
                            toLeftPoints++
                            break
                        } else {
                            toLeftPoints++
                        }
                    }
                }
                // To TOP
                var indexToTop = 0
                var toTopPoints = 0
                while (true) {
                    indexToTop++
                    if (currentTreeHeight != null) {
                        if (arrayOfTrees.getOrNull(rowIndex-indexToTop)  == null || arrayOfTrees.getOrNull(rowIndex-indexToTop)!!.getOrNull(colIndex) == null) {
                            break
                        }
                        var heightOfTreeToDirection = arrayOfTrees[rowIndex-indexToTop][colIndex]!!
                        if ((currentTreeHeight <= heightOfTreeToDirection)) {
                            // END when heigheter tree
                            // and add some points
                            toTopPoints++
                            break
                        } else {
                            toTopPoints++
                        }
                    }
                }
                // To BOTTOM
                var indexToBottom = 0
                var toBottomPoints = 0
                while (true) {
                    indexToBottom++
                    if (currentTreeHeight != null) {
                        if (arrayOfTrees.getOrNull(rowIndex+indexToBottom)  == null || arrayOfTrees.getOrNull(rowIndex+indexToBottom)!!.getOrNull(colIndex) == null) {
                            break
                        }
                        var heightOfTreeToDirection = arrayOfTrees[rowIndex+indexToBottom][colIndex]!!
                        if ((currentTreeHeight <= heightOfTreeToDirection)) {
                            // END when heigheter tree
                            // and add some points
                            toBottomPoints++
                            break
                        } else {
                            toBottomPoints++
                        }
                    }
                }

                // ---------
                arrayOfTreesCounted[rowIndex][colIndex] = toTopPoints * toBottomPoints * toLeftPoints * toRightPoints
            }
        }



        var answer = 0
        arrayOfTreesCounted.forEach { ints ->
            ints.forEach { value ->
                if (value == null) {
//                    print("   ")
                } else {
//                    print(" $value ")
                }

            }
            if (answer < ints.maxOf { it?.toInt() ?: 0 })
                answer = ints.maxOf { it?.toInt() ?: 0 }
//            println()
        }


        return answer.toString()
    }
}

