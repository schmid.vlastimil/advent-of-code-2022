package services.daysOfCode

import java.awt.Point
import java.util.LinkedList
import java.util.Queue
import kotlin.system.measureTimeMillis

/**
 *
 *                        ,sdPBbs.
 *                      ,d$$$$$$$$b.
 *                     d$P'`Y'`Y'`?$b
 *                    d'    `  '  \ `b
 *                   /    |        \  \
 *                  /    / \       |   \
 *             _,--'        |      \    |
 *           /' _/          \   |        \
 *        _/' /'             |   \        `-.__
 *    __/'       ,-'    /    |    |     \      `--...__
 *  /'          /      |    / \     \     `-.           `\
 * /    /;;,,__-'      /   /    \            \            `-.
 *
 */
class DayOfCode12: ADayOfCode() {
    override var dayNumber = 12

    override fun makePartOne(): String {
        val mapChar = makeMap()
        val startPoint = getStartPoint(mapChar)
        val endPoint = getEndPoint(mapChar)

        return algorithm(mapChar, startPoint, endPoint).toString()
    }

    override fun makePartTwo(): String {
        val mapChar = makeMap()
        val startPoints = getPart2StartPoints(mapChar)
        val endPoint = getEndPoint(mapChar)

        var shortPath = Int.MAX_VALUE
        startPoints.forEach { startPoint ->
            val result = algorithm(mapChar, startPoint, endPoint)
            if (result < shortPath && result != -1)
                shortPath = result
        }

        return shortPath.toString()
    }

    fun makeMap(): MutableList<MutableList<Char>> {
        val inputData = getInputData(1)

        val rowsCount = inputData.split("\n").count()
        val colsCount = inputData.split("\n").first().count()

        val mapChar = MutableList(rowsCount) { MutableList(colsCount) { '0' } }

        inputData.split("\n").forEachIndexed { rowIndex, rowString ->
            rowString.forEachIndexed { colIndex, colChar ->
                mapChar[rowIndex][colIndex] = colChar
            }
        }
        return mapChar
    }

    fun getStartPoint(map: MutableList<MutableList<Char>>): Point {
        map.forEachIndexed { rowIndex, row ->
            row.forEachIndexed { colIndex, col ->
                if (col == 'S') return Point(rowIndex, colIndex)
            }
        }
        return Point(0,0)
    }

    fun getEndPoint(map: MutableList<MutableList<Char>>): Point {
        map.forEachIndexed { rowIndex, row ->
            row.forEachIndexed { colIndex, col ->
                if (col == 'E') return Point(rowIndex, colIndex)
            }
        }
        return Point(0,0)
    }

    fun getPart2StartPoints(map: MutableList<MutableList<Char>>): List<Point> {
        val endPoints = MutableList(0) { Point() }

        map.forEachIndexed { rowIndex, row ->
            row.forEachIndexed { colIndex, col ->
                if (col == 'a' || col == 'S')
                    endPoints.add(Point(rowIndex, colIndex))
            }
        }
        return endPoints.toList()
    }

    fun algorithm(map: MutableList<MutableList<Char>>, startPoint: Point, endPoint: Point): Int {
        val queue: Queue<Pair<Point, Int>> = LinkedList()
        queue.add(Pair(startPoint, 0))

        val visited = hashSetOf<Point>()
        visited.add(startPoint)

        while (queue.isNotEmpty()) {
            val (nodePoint, distance) = queue.remove()
            if (nodePoint == endPoint) {
                // when done
                return distance
            }

            var currentElevation = map[nodePoint.x][nodePoint.y]
            if (currentElevation == 'S') {
                currentElevation = 'a'
            }

            fun visitNeighbour(currentPosition: Point) {
                // test if indices exists
                if (
                    map.getOrNull(currentPosition.x) == null ||
                    map[currentPosition.x].getOrNull(currentPosition.y) == null
                ) {
                    return
                }

                var nextElevation = map[currentPosition.x][currentPosition.y]
                if (nextElevation == 'E')  {
                    nextElevation = 'z'
                }

                if ((currentElevation - nextElevation) >= -1) {
                    if (!visited.contains(currentPosition)) {
                        queue.add(Pair(currentPosition, distance + 1))
                        visited.add(currentPosition)
                    }
                }
            }
            visitNeighbour(Point(nodePoint.x-1,nodePoint.y))
            visitNeighbour(Point(nodePoint.x+1,nodePoint.y))
            visitNeighbour(Point(nodePoint.x,nodePoint.y-1))
            visitNeighbour(Point(nodePoint.x,nodePoint.y+1))
        }

        return -1
    }
}

