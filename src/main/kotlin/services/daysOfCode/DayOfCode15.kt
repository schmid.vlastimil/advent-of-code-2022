package services.daysOfCode

import java.awt.Point
import kotlin.math.abs
import kotlin.math.max

class DayOfCode15: ADayOfCode() {
    override var dayNumber = 15

    override fun makePartOne(): String {
        val sensors = parseData()

        val noPossibleBeacons = HashSet<String>(0)
        val beacons = MutableList(0) { "" }

        val yLine = 2_000_000
//        val yLine = 10
        sensors.forEach { sensor ->
            val sensorP = sensor.first
            val beaconP = sensor.second
            beacons.add("${beaconP.x},${beaconP.y}")
            val dist = manhattan(sensorP, beaconP)

            val forFrom = (sensorP.x - dist)
            val forTo = (sensorP.x + dist + 1)

            for (x in forFrom until forTo) {
                if (manhattan(sensorP, Point(x, yLine)) <= dist) {
                    noPossibleBeacons.add("${x},${yLine}")
                }
            }

        }

        return (noPossibleBeacons subtract beacons.toSet()).count().toString()
    }

    override fun makePartTwo(): String {
        val sensors = parseData()

        val missingFrequency = getMissingFrequency(sensors, 4_000_000)

        return missingFrequency.toString()
    }

    fun parseData(): MutableList<Pair<Point, Point>> {
        val sensors = MutableList(0) { Pair(Point(), Point()) }
        val inputData = getInputData(1)
            .replace(",", "")
            .replace(":", "")
            .replace("x=", "")
            .replace("y=", "")

        inputData.split("\n").forEach { line ->
            val splits = line.split(' ')
            val sensorX = splits[2].toInt()
            val sensorY = splits[3].toInt()

            val beaconX = splits[8].toInt()
            val beaconY = splits[9].toInt()

            sensors.add(Pair(Point(sensorX, sensorY) , Point(beaconX, beaconY)))
        }

        return sensors
    }

    /**
     * Radius
     */
    fun manhattan(beaconP: Point, sensorP: Point): Int {
        return abs((beaconP.x - sensorP.x)) + abs(beaconP.y - sensorP.y)
    }

    fun rowRangeCoverage(sensors: MutableList<Pair<Point, Point>>, row: Int): MutableList<Pair<Int,Int>> {
        val intervals = MutableList(0) { Pair(0,0) }
        sensors.forEach {
            val sensorP = it.first
            val beaconP = it.second
            val manhattan = manhattan(sensorP, beaconP)
            val yDistanceOffset = abs(sensorP.y - row)
            if (yDistanceOffset <= manhattan) {
                val start = sensorP.x - (manhattan - yDistanceOffset)
                val end = sensorP.x + (manhattan - yDistanceOffset)
                intervals.add(Pair(start, end))
            }
        }
        return intervals
    }

    fun getMissingFrequency(sensors: MutableList<Pair<Point, Point>>, maxWidth: Int): Long {
        for (row in 0 .. maxWidth) {
            var maxCoverage = 0
            val intervals = rowRangeCoverage(sensors, row)
            intervals.sortBy { it.first }
            intervals.forEach { interval ->
                val start = interval.first
                val end = interval.second
                if (start > maxCoverage + 1) {
                    return maxWidth.toLong() * (start.toLong() - 1) + row.toLong()
                }
                maxCoverage = max(maxCoverage, end)
                if (maxCoverage > maxWidth) {
                    return@forEach
                }
            }
        }
        return -1
    }
}
