package services.daysOfCode

class DayOfCode05: ADayOfCode() {
    override var dayNumber = 5

    override fun makePartOne(): String {
        val inputData = getInputData(1)
        val stockpile = getStockpile(inputData)

        inputData.split("\n\n")
            .toMutableList()
            .removeLast()
            .split('\n')
            .forEach {  command ->
                val cmdSplit = command.split(' ')
                val count = cmdSplit[1].toInt()
                val fromIndex = cmdSplit[3].toInt() - 1
                val toIndex = cmdSplit[5].toInt() - 1
                moveLastElementFromTo(stockpile[fromIndex] , stockpile[toIndex], count)
            }

        return getAnswerFromSortedStockpile(stockpile)
    }

    override fun makePartTwo(): String {
        val inputData = getInputData(1)
        val stockpile = getStockpile(inputData)

        inputData.split("\n\n")
            .toMutableList()
            .removeLast()
            .split('\n')
            .forEach {  command ->
                val cmdSplit = command.split(' ')
                val count = cmdSplit[1].toInt()
                val fromIndex = cmdSplit[3].toInt() - 1
                val toIndex = cmdSplit[5].toInt() - 1
                moveLastElementFromTo9001(stockpile[fromIndex] , stockpile[toIndex], count)
            }

        return getAnswerFromSortedStockpile(stockpile)
    }

    fun getAnswerFromSortedStockpile(stockpile: Array<MutableList<String>>): String {
        var answer = ""
        stockpile.forEach {
            answer += it.last()
        }
        return answer
    }
    fun moveLastElementFromTo(from: MutableList<String>, to: MutableList<String>, count: Int) {
        for (index in 0 until count) {
            to.add(from.last())
            from.removeLast()
        }
    }

    fun moveLastElementFromTo9001(from: MutableList<String>, to: MutableList<String>, count: Int) {
        val stackToMove = mutableListOf<String>()
        for (index in 0 until count) {
            stackToMove.add(from[from.count() - index - 1])
        }

        stackToMove.reverse()
        to.addAll(stackToMove)
        for (index in 0 until count) {
            from.removeLast()
        }
    }

    fun getStockpile(inputData: String): Array<MutableList<String>> {
        val stockStringRows = inputData.split("\n\n")
            .map { it.split('\n') }
            .first()
            .toMutableList()
        val maxStacks = stockStringRows.last()
            .split(' ')
            .last()
            .toInt()
        stockStringRows.removeLast()

        val stockpile = Array(maxStacks) { mutableListOf<String>() }
        stockStringRows.asReversed()
            .forEach { row ->
                for (charIndex in 1 ..  row.count() step 4 ) {
                    if (row.indices.contains(charIndex) && charIndex != 0) {
                        val stockpileIndex = charIndex / 4
                        if (row[charIndex].toString() != " ")
                            stockpile[stockpileIndex].add(row[charIndex].toString())
                    }
                }
            }

        stockpile.toMutableList()
        return stockpile
    }
}

