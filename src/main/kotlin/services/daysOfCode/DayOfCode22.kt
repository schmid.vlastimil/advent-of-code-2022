package services.daysOfCode

import java.awt.Point
import java.lang.Exception

class DayOfCode22: ADayOfCode() {
    override var dayNumber = 22

    /**
     * Answer 1: 109094
     * Done in: 21ms
     * Answer 2: 53324
     * Done in: 6ms
     */
    override fun makePartOne(): String {
        val inputData = getInputData(1)
        val split = inputData.split("\n\n")
        val cmds = split[1].replace("L", " L ").replace("R", " R ").split(" ")
        val map = MonkeyMap(split[0])

        return solve(map, cmds)
    }

    override fun makePartTwo(): String {
        val inputData = getInputData(1)
//            .replace("#", "X")
        val split = inputData.split("\n\n")
        var cmds = split[1].replace("L", " L ").replace("R", " R ").split(" ")
        val map = MonkeyMapPart2(split[0])
        map.loadTeleports()

//
//        cmds = listOf(
//            "R","190"
//        )
        return solve(map, cmds)
    }

    fun solve(map: MonkeyMap, cmds: List<String>): String {
        val path = mutableListOf(map.getStartingPathNode())

        cmds.forEach {
            if (it == "R") {
                rotateLast(path, true)
            } else if (it == "L") {
                rotateLast(path, false)
            } else {
                path += move(path.last(),it.toInt(), map)
            }
        }
//        map.render(path)
        return getPassword(path.last()).toString()
    }
    fun getPassword(node: MapPathNode): Int {
        val finalColumn = node.point.x + 1
        val finalRow = node.point.y + 1
        val finalFacing = when (node.directionFacing) {
            DirectionFacing.RIGHT -> 0
            DirectionFacing.DOWN -> 1
            DirectionFacing.LEFT -> 2
            DirectionFacing.UP -> 3
        }
        val answer = (1000 * finalRow) + (4 * finalColumn) + finalFacing
        return answer
    }

    fun rotateLast(nodes: List<MapPathNode>, clockWise: Boolean) {
        val nexDirection: DirectionFacing
        if (clockWise) {
            nexDirection = when(nodes.last().directionFacing) {
                DirectionFacing.RIGHT -> DirectionFacing.DOWN
                DirectionFacing.DOWN  -> DirectionFacing.LEFT
                DirectionFacing.LEFT  -> DirectionFacing.UP
                DirectionFacing.UP    -> DirectionFacing.RIGHT
            }
        } else {
            nexDirection = when(nodes.last().directionFacing) {
                DirectionFacing.RIGHT -> DirectionFacing.UP
                DirectionFacing.UP    -> DirectionFacing.LEFT
                DirectionFacing.LEFT  -> DirectionFacing.DOWN
                DirectionFacing.DOWN  -> DirectionFacing.RIGHT
            }
        }

        nodes.last().directionFacing = nexDirection
    }

    fun move(from: MapPathNode, steps: Int, map: MonkeyMap): List<MapPathNode> {
        val newPaths = mutableListOf<MapPathNode>()
        var fromPoint = Point(from.point)

        run cycle@ {
            repeat(steps) {
                val translateBy: Point = when(from.directionFacing) {
                    DirectionFacing.RIGHT -> Point(1,0)
                    DirectionFacing.LEFT -> Point(-1,0)
                    DirectionFacing.UP -> Point(0,-1)
                    DirectionFacing.DOWN -> Point(0,1)
                }
                fromPoint.translate(translateBy.x, translateBy.y)
                if (map.isVoid(fromPoint)) {
                    map.wrapPoint(fromPoint, from.directionFacing, from)
                }
                if (map.isSolid(fromPoint)) {
                    return@cycle
                }

                if (map.isOutOfBounds(fromPoint) || map.isVoid(fromPoint)) {
//                    throw Exception("Path is out of map in >> ${fromPoint} ${from.directionFacing}")
                } else {
                    newPaths.add(MapPathNode(Point(fromPoint), from.directionFacing))
                }
            }
        }

        return newPaths
    }

    enum class DirectionFacing() { LEFT, RIGHT, UP, DOWN }

    data class MapPathNode(val point: Point, var directionFacing: DirectionFacing)

    class MonkeyMapPart2(mapString: String): MonkeyMap(mapString) {
        val teleports = listOf<WrapTeleport>()

        fun isEdgeOfSqr(x: Int, y: Int, point: Point, isHorizontal: Boolean = true): Boolean {
            val sizeOfEdge = 50
            if (isHorizontal) {
                val minY = y*sizeOfEdge - 1
                val maxY = y*sizeOfEdge - 0
                val minX = sizeOfEdge * x
                val maxX = sizeOfEdge * (x + 1) - 1

                return point.y in minY .. maxY &&
                        point.x in minX .. maxX &&
                        isVoid(point)
            } else {
                val minX = x*sizeOfEdge - 1
                val maxX = x*sizeOfEdge - 0
                val minY = sizeOfEdge * y
                val maxY = sizeOfEdge * (y + 1) - 1

                return point.y in minY .. maxY &&
                        point.x in minX .. maxX &&
                        isVoid(point)
            }
        }
        override fun wrapPoint(point: Point, direction: DirectionFacing, node: MapPathNode) {
            /**
             * CUBE:
             *     ...15.
             *     ...4..
             *     ..26..
             *     ..3...
             */
            val sizeOfEdge = 50
            var newDirection = direction
            if (direction == DirectionFacing.DOWN || direction == DirectionFacing.UP) {
                // 1 - Horní
                if (isEdgeOfSqr(1,0, point, true)) {
                    newDirection = DirectionFacing.RIGHT
                    point.y = (sizeOfEdge * 3 - 1) + 1 + point.x - sizeOfEdge
                    point.x = 0
                }
                // 5 - Horní
                else if (isEdgeOfSqr(2,0, point, true)) {
                    // překvapivě stejná direction
                    newDirection = DirectionFacing.UP
                    point.y = mapList.count() - 1
                    point.x = point.x - sizeOfEdge * 2
                }
                // 2 - Horní
                else if (isEdgeOfSqr(0,2, point, true)) {
                    newDirection = DirectionFacing.RIGHT
                    point.y = sizeOfEdge * 1 + point.x
                    point.x = sizeOfEdge * 1
                }
                // 3 - Dolní
                else if (isEdgeOfSqr(0,4, point, true)) {
                    newDirection = DirectionFacing.DOWN
                    point.y = 0
                    point.x = sizeOfEdge * 2 + point.x
                }
                // 6 - Dolní
                else if (isEdgeOfSqr(1,3, point, true)) {
                    newDirection = DirectionFacing.LEFT
                    point.y = sizeOfEdge * 3 + (point.x - sizeOfEdge)
                    point.x = sizeOfEdge * 1 - 1
                }
                // 5 - Dolní
                else if (isEdgeOfSqr(2,1, point, true)) {
                    newDirection = DirectionFacing.LEFT
                    point.y = sizeOfEdge * 1 + (point.x.mod(sizeOfEdge))
                    point.x = sizeOfEdge * 2 - 1
                }
            }

            else {
                // 1 - Levá
                if (isEdgeOfSqr(1,0, point, false)) {
                    newDirection = DirectionFacing.RIGHT
                    point.y = sizeOfEdge * 3 - point.y.mod(sizeOfEdge) - 1
                    point.x = 0
                }
                // 2 - Levá
                if (isEdgeOfSqr(0,2, point, false)) {
                    newDirection = DirectionFacing.RIGHT
                    point.y = sizeOfEdge - point.y.mod(sizeOfEdge) - 1
                    point.x = sizeOfEdge
                }
                // 3 - Levá
                if (isEdgeOfSqr(0,3, point, false)) {
                    newDirection = DirectionFacing.DOWN
                    point.x = sizeOfEdge + point.y.mod(sizeOfEdge)
                    point.y = 0
                }
                // 4 - Levá
                if (isEdgeOfSqr(1,1, point, false)) {
                    newDirection = DirectionFacing.DOWN
                    point.x = point.y.mod(sizeOfEdge)
                    point.y = sizeOfEdge * 2
                }
                // 5 - Pravá
                if (isEdgeOfSqr(3,0, point, false)) {
                    newDirection = DirectionFacing.LEFT
                    point.y = sizeOfEdge * 2 +  (sizeOfEdge - point.y.mod(sizeOfEdge)) - 1
                    point.x = sizeOfEdge * 2 - 1
                }
                // 4 - Pravá
                if (isEdgeOfSqr(2,1, point, false)) {
                    newDirection = DirectionFacing.UP
                    point.x = sizeOfEdge * 2 + point.y.mod(sizeOfEdge)
                    point.y = sizeOfEdge - 1
                }
                // 6 - Pravá
                if (isEdgeOfSqr(2,2, point, false)) {
                    newDirection = DirectionFacing.LEFT
                    point.y =  sizeOfEdge - point.y.mod(sizeOfEdge) - 1
                    point.x = sizeOfEdge * 3 - 1
                }
                // 3 - Pravá
                if (isEdgeOfSqr(1,3, point, false)) {
                    newDirection = DirectionFacing.UP
                    point.x = sizeOfEdge + point.y.mod(sizeOfEdge)
                    point.y = sizeOfEdge * 3 - 1
                }
            }

            if (!isSolid(point)) {
                node.directionFacing = newDirection
            }
        }

        fun loadTeleports() {

            var test = ""
        }
    }

    data class WrapTeleport(val from: Point, val to: Point, val fromDirectionFacing: DirectionFacing)

    open class MonkeyMap(mapString: String) {
        val mapList: MutableList<MutableList<Char>>
        init {
            val mapRows = mapString.split("\n")
            val xSize = mapRows.maxOf { it.count() }
            val ySize = mapRows.count()
            mapList = MutableList(ySize) { MutableList(xSize) { ' ' } }

            mapRows.forEachIndexed { indexY, row ->
                row.forEachIndexed { indexX, c ->
                    mapList[indexY][indexX] = c
                }
            }
        }

        open fun wrapPoint(point: Point, direction: DirectionFacing, node: MapPathNode) {
            when(direction) {
                DirectionFacing.RIGHT -> {
                    repeat(mapList.first().size) {
                        if (!isVoid(Point(it,point.y))) {
                            point.x = it
                            return
                        }
                    }
                }
                DirectionFacing.LEFT ->  {
                    for (x in mapList.first().size downTo 0) {
                        if (!isVoid(Point(x,point.y))) {
                            point.x = x
                            return
                        }
                    }
                }
                DirectionFacing.UP ->    {
                    for (y in mapList.size downTo 0) {
                        if (!isVoid(Point(point.x,y))) {
                            point.y = y
                            return
                        }
                    }
                }
                DirectionFacing.DOWN ->  {
                    repeat(mapList.size) {
                        if (!isVoid(Point(point.x, it))) {
                            point.y = it
                            return
                        }
                    }
                }
            }
        }

        fun isVoid(point: Point): Boolean {
            return isOutOfBounds(point) || getChar(point) == ' '
        }

        fun isSolid(point: Point): Boolean {
            return !isOutOfBounds(point) && getChar(point) == '#'
        }

        fun getChar(point: Point): Char {
            return mapList[point.y][point.x]
        }

        fun isOutOfBounds(point: Point): Boolean {
            return mapList.getOrNull(point.y) == null || mapList[point.y].getOrNull(point.x) == null
        }

        fun getStartingPathNode(): MapPathNode {
            var x = 0
            kotlin.run cycleEnd@ {
                mapList.last().forEachIndexed { indexX, it ->
                    if (getChar(Point(indexX, 0)) == '.') {
                        x = indexX
                        return@cycleEnd
                    }
                }
            }

//            return MapPathNode(Point(50, 36), DirectionFacing.RIGHT)
            return MapPathNode(Point(x, 0), DirectionFacing.RIGHT)
        }

        fun render(path: List<MapPathNode>) {
            renderXNumbers()

            mapList.forEachIndexed { indexY, chars ->
                print((indexY).toString().padEnd(3))
                chars.forEachIndexed { indexX, c ->
                    val node = path.find { it.point.x == indexX && it.point.y == indexY }
                    if (path.first().point.x == indexX && path.first().point.y == indexY) {
                        print("S")
                    } else if (node != null) {
                        val charToPrint: Char = when(node.directionFacing) {
                            DirectionFacing.RIGHT -> '>'
                            DirectionFacing.LEFT -> '<'
                            DirectionFacing.UP -> '^'
                            DirectionFacing.DOWN -> 'v'
                        }
                        print(charToPrint)
                    } else {
                        print(c)
                    }
                }
                println()
            }

            renderXNumbers()
        }

        private fun renderXNumbers() {
            val elseChar = " "
            val eachXNum = 2

            print("   ")
            for (number in 0 .. mapList.first().count()) {
                if (number % eachXNum == 0) {
                    print(number.toString().getOrElse(0, elseChar))
                } else {
                    print(" ")
                }
            }
            println()

            print("   ")
            for (number in 0 .. mapList.first().count()) {
                if (number % eachXNum == 0) {
                    print(number.toString().getOrElse(1, elseChar))
                } else {
                    print(" ")
                }
            }
            println()

            print("   ")
            for (number in 0 .. mapList.first().count()) {
                if (number % eachXNum == 0) {
                    print(number.toString().getOrElse(2, elseChar))
                } else {
                    print(" ")
                }
            }
            println()

        }
    }

}

private fun String.getOrElse(index: Int, defaultValue: String): String {
    return if (index >= 0 && index <= lastIndex) get(index).toString() else defaultValue
}



