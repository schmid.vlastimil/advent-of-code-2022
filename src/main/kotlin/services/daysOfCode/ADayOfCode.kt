package services.daysOfCode
import java.io.File

abstract class ADayOfCode {
    abstract var dayNumber: Int

    abstract fun makePartOne(): String?
    abstract fun makePartTwo(): String?

    fun getAnswer(part: Int = 1): String? {
        return when(part) {
            1 -> { makePartOne() }
            2 -> { makePartTwo() }
            else -> { null }
        }
    }

    protected fun getInputData(part: Int = 1): String {
        var fileName = "";
        if (part != 1) {
            fileName = "${this.dayNumber}-${part}.txt"
        } else {
            fileName = "${this.dayNumber}.txt"
        }
        val filePath = "storage/inputTxtFiles/${fileName}"
        val classLoader = Thread.currentThread().contextClassLoader

        val url = classLoader.getResource(filePath)

        val text = File(url.toURI()).readText()
        return text
    }
    protected fun getInputDataReadLines(part: Int = 1): List<String>  {
        var fileName = "";
        if (part != 1) {
            fileName = "${this.dayNumber}-${part}.txt"
        } else {
            fileName = "${this.dayNumber}.txt"
        }
        val filePath = "storage/inputTxtFiles/${fileName}"
        val classLoader = Thread.currentThread().contextClassLoader

        val url = classLoader.getResource(filePath)

        val text = File(url.toURI()).readLines()
        return text
    }
}