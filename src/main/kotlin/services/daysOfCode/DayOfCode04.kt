package services.daysOfCode

class DayOfCode04: ADayOfCode() {
    override var dayNumber = 4

    override fun makePartOne(): String {
        val inputData = getInputData(1)
        var fullContainCount = 0

        val pairsInRows = inputData.split('\n')
            .map { it.split(',') }
            .map { it.map { it.split('-').map { it.toInt() } } }

        pairsInRows.forEach {pairs ->
            val pairRange0 = createListFromTo(pairs[0][0], pairs[0][1]).toSet()
            val pairRange1 = createListFromTo(pairs[1][0], pairs[1][1]).toSet()
            if (pairRange0.containsAll(pairRange1) || pairRange1.containsAll(pairRange0))
                fullContainCount++
        }

        return fullContainCount.toString()
    }

    override fun makePartTwo(): String {
        val inputData = getInputData(1);
        var fullContainCount = 0

        val pairsInRows = inputData.split('\n')
            .map { it.split(',') }
            .map { it.map { it.split('-').map { it.toInt() } } }

        pairsInRows.forEach{ pairs ->
            val pairRange0 = createListFromTo(pairs[0][0], pairs[0][1]).toSet()
            val pairRange1 = createListFromTo(pairs[1][0], pairs[1][1]).toSet()
            if (pairRange0.intersect(pairRange1).isNotEmpty())
                fullContainCount++
        }

        return fullContainCount.toString()
    }

    fun createListFromTo(from: Int, to: Int): List<Int> {
        var rangeList = emptyList<Int>()

        for (i in from..to) {
            rangeList = rangeList.plus(i)
        }
        return rangeList.toList()
    }

}

