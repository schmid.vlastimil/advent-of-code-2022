package services.daysOfCode

import java.awt.Point
import kotlin.math.round

var roundNumber = 0

private fun Point.getTranslated(point: Point): Point {
    translate(point.x, point.y)
    return Point(x, y)
}

//Answer 1: 3874
//Done in: 227ms
//Answer 2: 948
//Done in: 1872ms
class DayOfCode23: ADayOfCode() {
    override var dayNumber = 23

    override fun makePartOne(): String {
        val inputData = getInputData(1)
        val map = AoCMap(inputData)

//        println("== End of Round ${roundNumber} ==")
        repeat(10) {
            makeRound(map)
//            println("== End of Round ${roundNumber} ==")
//            map.render()
        }
//        map.render()

        kotlin.run {
            for (x in map.mapList.size - 1 downTo 0) {
                if (!map.mapList[x].contains('#')) {
                    map.mapList.removeAt(x)
                } else {
                    return@run
                }
            }
        }

        map.mapList.reverse()
        kotlin.run {
            for (x in map.mapList.size - 1 downTo 0) {
                if (!map.mapList[x].contains('#')) {
                    map.mapList.removeAt(x)
                } else {
                    return@run
                }
            }
        }
        map.mapList.reverse()

        kotlin.run {
            var y = 0
            while (true) {
                for (x in 0 until map.mapList.size) {
                    if (map.mapList[x][y] == '#') {
                        return@run
                    }
                }
                for (x in 0 until map.mapList.size) {
                    map.mapList[x].removeFirst()
                }
            }
        }

        map.mapList.forEach { it.reverse() }
        kotlin.run {
            var y = 0
            while (true) {
                for (x in 0 until map.mapList.size) {
                    if (map.mapList[x][y] == '#') {
                        return@run
                    }
                }
                for (x in 0 until map.mapList.size) {
                    map.mapList[x].removeFirst()
                }
            }
        }
        map.mapList.forEach { it.reverse() }


//        map.render()

        var emptySpace = map.countEmptyTiles()

        return emptySpace.toString()
    }

    override fun makePartTwo(): String {
        roundNumber = 0
        val inputData = getInputData(1)
        val map = AoCMap(inputData)

        repeat(1000) {
            makeRound(map)
        }

//        map.render()

        return (maxRoundNumberMoved + 1).toString()
    }

    fun makeRound(map: AoCMap) {
        roundNumber++
        // Elf Current Point, Elf Next potentional Point
        val elfMoves = mutableListOf<Pair<Elf,Point>>()
        map.elfs.forEach { elf ->
            if (map.hasAnyNeighbours(elf)) {
                var elfMove = map.getMoveTo(elf)
                if (elfMove != null) {
                    elfMoves.add(Pair(elf, elfMove))
                }
            }
        }
        // Remove duplicit moves
        var toRemoveMoves = elfMoves.groupBy { it.second.toString() }
        var toDeleteIndices = hashSetOf<Int>()
        toRemoveMoves.forEach {
            if (it.value.count() == 1) {
                // DO NOT DELETE
            } else {
                // Delete
                var test = ""
                elfMoves.forEachIndexed { index, pair ->
                    if (pair.second.toString() == it.value.first().second.toString()) {
                        toDeleteIndices.add(index)
                    }
                }
            }
        }
        var toDeleteIndicesSorted = toDeleteIndices.sortedDescending()
        toDeleteIndicesSorted.forEach { elfMoves.removeAt(it) }

        // MOVE elfs
        elfMoves.forEach {
            map.mapList[it.first.point.x][it.first.point.y] = '.'
            map.mapList[it.second.x][it.second.y] = '#'

            it.first.moveTo(it.second)
            if (maxRoundNumberMoved != roundNumber) {
//                println(roundNumber)
            }
            maxRoundNumberMoved = roundNumber
//            println(roundNumber)
        }

        map.elfs.forEach {
            it.cycleQueue()
        }

        var test = ""
    }
    var maxRoundNumberMoved = 0

    enum class Direction() {
        NORTH      { override fun getPoint(): Point { return Point(0,1) } },
        EAST       { override fun getPoint(): Point { return Point(1,0) } },
        SOUTH      { override fun getPoint(): Point { return Point(0,-1) } },
        WEST       { override fun getPoint(): Point { return Point(-1,0) } },
        NORTH_EAST { override fun getPoint(): Point { return Point(1,1) } },
        SOUTH_EAST { override fun getPoint(): Point { return Point(1,-1) } },
        SOUTH_WEST { override fun getPoint(): Point { return Point(-1,-1) } },
        NORTH_WEST { override fun getPoint(): Point { return Point(-1,1) } };

        abstract fun getPoint(): Point
    }



    open class AoCMap(mapString: String) {
        val mapList: MutableList<MutableList<Char>>
        val elfs: MutableList<Elf>
        init {
            val offset = 2000
            val mapRows = mapString.split("\n")
            val xSize = mapRows.maxOf { it.count() }
            val ySize = mapRows.count()
            mapList = MutableList(xSize + offset) { MutableList(ySize + offset) { '-' } }

            for (x in 0 until xSize) {
                for (y in 0 until ySize) {
                    mapList[x+offset/2][y+offset/2] = mapRows[y][x]
                }
            }
            mapList.forEach { it.reverse() }

            // Elf Load
            elfs = mutableListOf()
            mapList.forEachIndexed { x, chars ->
                chars.forEachIndexed { y, c ->
                    if (c == '#') {
                        elfs.add(Elf(Point(x,y)))
                    }
                }
            }
        }

        val neighboursDirections = listOf(Direction.NORTH, Direction.SOUTH, Direction.WEST, Direction.EAST,
            Direction.NORTH_EAST, Direction.NORTH_WEST, Direction.SOUTH_WEST, Direction.SOUTH_EAST)
        fun hasAnyNeighbours(elf: Elf): Boolean {
            neighboursDirections.forEach { direction ->
                var pointToTest = Point(elf.point)
                pointToTest.translate(direction.getPoint().x, direction.getPoint().y)

                if (!isOutOfBounds(pointToTest) && isElf(pointToTest)) {
                    return true
                }
            }
            return false
        }

        fun countEmptyTiles(): Int {
            var emptySpace = 0
            mapList.forEach { chars ->
                chars.forEach {
                    if (it == '.' || it == '-') {
                        emptySpace++
                    }
                }
            }
            return emptySpace
        }

        fun isElf(point: Point): Boolean {
            return getChar(point) == '#'
        }

        fun isEmpty(point: Point): Boolean {
            return getChar(point) == '.'
        }

        fun isOutOfBounds(point: Point): Boolean {
            return mapList.getOrNull(point.x) == null || mapList[point.x].getOrNull(point.y) == null
        }

        fun getElfPositions(): List<Point> {
            val elfs = mutableListOf<Point>()
            mapList.forEachIndexed { x, chars ->
                chars.forEachIndexed { y, c ->
                    if (c == '#')
                        elfs.add(Point(x,y))
                }
            }
            return elfs
        }

        fun isElfDirectionsFree(point: Point, directions: List<Direction>): Boolean {
            directions.forEach { direction ->
                var nextPoint = Point(point).getTranslated(direction.getPoint())
                if (isOutOfBounds(nextPoint) || isElf(nextPoint))
                    return false
            }
            return true
        }

        fun getMoveTo(elf: Elf): Point? {
            val point = elf.point
            var move: Point? = null
            var direction: Direction? = null

            kotlin.run elfDirCycle@{
                elf.directionQueue.forEach { directionElf ->
                    if (directionElf == Direction.NORTH && isElfDirectionsFree(point, listOf(Direction.NORTH, Direction.NORTH_EAST, Direction.NORTH_WEST))) {
                        direction = Direction.NORTH
                        return@elfDirCycle
                    } else if (directionElf == Direction.SOUTH && isElfDirectionsFree(point, listOf(Direction.SOUTH, Direction.SOUTH_EAST, Direction.SOUTH_WEST))) {
                        direction = Direction.SOUTH
                        return@elfDirCycle
                    } else if (directionElf == Direction.WEST && isElfDirectionsFree(point, listOf(Direction.WEST, Direction.NORTH_WEST, Direction.SOUTH_WEST))) {
                        direction = Direction.WEST
                        return@elfDirCycle
                    } else if (directionElf == Direction.EAST && isElfDirectionsFree(point, listOf(Direction.EAST, Direction.NORTH_EAST, Direction.SOUTH_EAST))) {
                        direction = Direction.EAST
                        return@elfDirCycle
                    } else {
                        var test = ""
                    }
                }
            }



            if (direction != null) {
                move = Point(point)
                move.translate(direction!!.getPoint().x, direction!!.getPoint().y)
                elf.consideredDirection = direction
            } else {
//                println("direction WAS null")
            }

            return move
        }

        fun getChar(point: Point): Char {
            return mapList[point.x][point.y]
        }


        fun render() {
            renderXNumbers()
            mapList.forEach { it.reverse() }

            for (y in 0 until mapList.first().size) {
                print((mapList.first().size - y - 1 ).toString().padEnd(3))
                for (x in 0 until mapList.size) {
                    var toPrint = ' '
                    toPrint = mapList[x][y]
                    print(toPrint)
                }
                println()
            }

            mapList.forEach { it.reverse() }
            renderXNumbers()
        }

        private fun renderXNumbers() {
            val elseChar = " "
            val eachXNum = 2

            print("   ")
            for (number in 0 .. mapList.first().count()) {
                if (number % eachXNum == 0) {
                    print(number.toString().getOrElse(0, elseChar))
                } else {
                    print(" ")
                }
            }
            println()

            print("   ")
            for (number in 0 .. mapList.first().count()) {
                if (number % eachXNum == 0) {
                    print(number.toString().getOrElse(1, elseChar))
                } else {
                    print(" ")
                }
            }
            println()

            print("   ")
            for (number in 0 .. mapList.first().count()) {
                if (number % eachXNum == 0) {
                    print(number.toString().getOrElse(2, elseChar))
                } else {
                    print(" ")
                }
            }
            println()

        }
    }

    class Elf(val point: Point) {
        var consideredDirection: DayOfCode23.Direction? = null
        var directionQueue: MutableList<DayOfCode23.Direction> = mutableListOf(DayOfCode23.Direction.NORTH, DayOfCode23.Direction.SOUTH, DayOfCode23.Direction.WEST, DayOfCode23.Direction.EAST)
        var lastRoundMoved  = 0
        fun moveTo(pointTo: Point) {
            if (point.x == pointTo.x && point.y == pointTo.y) {
                // did NOT moved
            } else {
                // MOVED
                this.lastRoundMoved++
            }
            point.x = pointTo.x
            point.y = pointTo.y
        }

        fun cycleQueue() {
            directionQueue.add(directionQueue.removeFirst())
        }
    }
}

private fun String.getOrElse(index: Int, defaultValue: String): String {
    return if (index >= 0 && index <= lastIndex) get(index).toString() else defaultValue
}

