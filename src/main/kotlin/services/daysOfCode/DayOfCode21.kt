package services.daysOfCode

import java.util.Locale
import kotlin.math.abs


//Answer 1: 276156919469632
//Done in: 19ms
//Answer 2: 3441198826073
//Done in: 285ms
class DayOfCode21: ADayOfCode() {
    override var dayNumber = 21

    override fun makePartOne(): String {

        val monkeys = makeMonkeys()
        val answer = solve(monkeys, "root")
        return answer.toString()
    }

    override fun makePartTwo(): String {
        var monkeys = makeMonkeys()

        val pairComp = getComparingMonkeyName()
        val compNonHumanName = pairComp.first
        val compHumanName = pairComp.second
        val compValue = solve(monkeys, compNonHumanName)
        println("WANTED>> ${compValue}")

        var value = 0.toLong()
        var changeBy = Long.MAX_VALUE/10000

        var smallestDiff = Long.MAX_VALUE

        var iteration = 0
        while (true) {
            var solvMinus = solveWithChangedHuman(value - changeBy, compHumanName)
            var solvPlus = solveWithChangedHuman(value + changeBy, compHumanName)

            var diffMinus = abs(solvMinus - compValue)
            var diffPlus = abs(solvPlus - compValue)

            if (diffPlus < smallestDiff) {
                value += changeBy
                smallestDiff = diffPlus
                println("${value.toString().padEnd(30, ' ')}${solvPlus} ChangeBy ${changeBy}; DIFF ${smallestDiff}")
            } else if (diffMinus < smallestDiff) {
                value -= changeBy
                smallestDiff = diffMinus
                println("${value.toString().padEnd(30, ' ')}${solvMinus} ChangeBy ${changeBy}; DIFF ${smallestDiff}")
            } else {
                if (changeBy < 100) {
                    changeBy--
//                    changeBy /= 2
                } else {
                    changeBy /= 2
                }
            }
            if (solvMinus == compValue) {
                break
            }

            iteration++
        }

        // 80164970870816 WANTED
        // 2918000000000000

        var answer = value
        return answer.toString()
    }

    fun solveWithChangedHuman(humanNumber: Long, humanPathName: String ): Long {
        var newMonkeys = makeMonkeys()
        newMonkeys["humn"]!!.number = humanNumber
        val solv = solve(newMonkeys, humanPathName)
        return solv
    }

    /**
     * gets value that human can change
     * First non human
     * Second Human
     */
    fun getComparingMonkeyName(): Pair<String,String> {
        var monkeys = makeMonkeys()
        val rootMonkey = monkeys["root"]
        val firstName = rootMonkey!!.monkeyNames!!.first
        val secondName = rootMonkey.monkeyNames!!.second

        val val1 = solve(monkeys, firstName)
        val val2 = solve(monkeys, secondName)

        monkeys = makeMonkeys()
        monkeys["humn"]!!.number = 0

        val val1Comp = solve(monkeys, firstName)
        val val2Comp = solve(monkeys, secondName)

        if (val1 == val1Comp) {
            return Pair(firstName, secondName)
        } else if (val2 == val2Comp) {
            return Pair(secondName, firstName)
        }

        return Pair("eeeee", "eeeee")
    }

    fun solve(monkeys: Map<String, Monkey>, name: String): Long {

        val monkey = monkeys[name]

        val expression = monkey!!.operation
        if (monkey.number != null) {
            return monkey.number!!
        }


        val operand1 = solve(monkeys, monkey.monkeyNames!!.first)
        val operand2 = solve(monkeys, monkey.monkeyNames!!.second)

        val result = when (expression) {
            MonkeyOperations.ADDITION -> operand1 + operand2
            MonkeyOperations.SUBTRACTION -> operand1 - operand2
            MonkeyOperations.MULTIPLICATION -> operand1 * operand2
            MonkeyOperations.DIVISION -> operand1 / operand2
            MonkeyOperations.YELL -> operand1 / operand2
            else -> throw Exception("error")
        }

        monkeys[name]!!.number = result

        return result
    }

    fun makeMonkeys(): Map<String, Monkey> {
        val inputData = getInputData(1).replace(":","")

        val monkeys = mutableMapOf<String, Monkey>()
        inputData.split("\n").forEach {
            monkeys[it.split(" ")[0]] = Monkey(it)
        }
        val monkeysMap = monkeys.toMap()
        return monkeysMap
    }

    enum class MonkeyOperations {
        DIVISION, ADDITION, MULTIPLICATION, SUBTRACTION, YELL
    }

    class Monkey(string: String) {
        var number: Long?
        val operation: MonkeyOperations
        var monkeyNames: Pair<String,String>?
        var name: String

        init {
            val split = string.split(" ")
            name = split[0]

            operation = when(split.getOrNull(2)) {
                "+" -> MonkeyOperations.ADDITION
                "-" -> MonkeyOperations.SUBTRACTION
                "/" -> MonkeyOperations.DIVISION
                "*" -> MonkeyOperations.MULTIPLICATION
                else -> MonkeyOperations.YELL
            }

            if (split.getOrNull(2) == null) {
                number = split[1].toLong()
                monkeyNames = null
            } else {
                monkeyNames = Pair(split[1],split[3])
                number = null
            }

        }

    }
}

