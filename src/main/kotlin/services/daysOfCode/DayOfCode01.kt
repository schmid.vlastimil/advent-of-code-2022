package services.daysOfCode

class DayOfCode01: ADayOfCode() {
    override var dayNumber = 1

    override fun makePartOne(): String {
        return getSummedEfls()
            .max()
            .toString();
    }

    override fun makePartTwo(): String {
        return getSummedEfls()
            .sortedDescending()
            .take(3)
            .sum()
            .toString();
    }

    fun getSummedEfls(): IntArray {
        val inputData = getInputData(1);

        val elfs = inputData.split("\n\n")
        val elfsSumed = IntArray(elfs.count())

        elfs.forEachIndexed  { indexElf, foodCal ->
            foodCal.split('\n')
                .forEach { elfsSumed[indexElf] += it.toInt() }
        }

        return elfsSumed;
    }
}

