package services.daysOfCode

import java.util.*
import kotlin.math.max

class DayOfCode19: ADayOfCode() {
    override var dayNumber = 19

    override fun makePartOne(): String {
        val inputData = getInputData(3)

        val blueprints = mutableListOf<Blueprint>()
        inputData.split('\n').forEach {
            blueprints.add(Blueprint(it))
        }

        var value = 0
        blueprints.forEachIndexed { index, blueprint ->
            value += blueprint.simulate(24) * (index + 1)
        }
        return value.toString()
    }

    override fun makePartTwo(): String {
        val inputData = getInputData(3)

        val blueprints = mutableListOf<Blueprint>()
        inputData.split('\n').forEach {
            blueprints.add(Blueprint(it))
        }

        var value = 1
        blueprints.forEachIndexed { index, blueprint ->
            if (index < 3)
                value *= blueprint.simulate(32)
        }
        return value.toString()
    }

    class Blueprint(blueprintString: String) {
        val oreRobot: OreRobot
        val clayRobot: ClayRobot
        val obsidianRobot: ObsidianRobot
        val geodeRobot: GeodeRobot

        init {
            val split = blueprintString.split(" ")
            oreRobot = OreRobot(Materials(ore = split[6].toInt()))
            clayRobot = ClayRobot(Materials(ore = split[12].toInt()))
            obsidianRobot = ObsidianRobot(Materials(ore = split[18].toInt(), clay = split[21].toInt()))
            geodeRobot = GeodeRobot(Materials(ore = split[27].toInt(), obsidian = split[30].toInt()))
        }

        fun simulate(minutes: Int): Int {
            //Materials, Robots, TimeRemaining
            val queue = LinkedList<BlueprintState>()
            queue.push(
                BlueprintState(Materials(),
                RobotCounts(ore = 1),
                minutes
            ))

            var visited = hashSetOf<String>()
            var best = 0


            val maxCostOre = listOf(oreRobot, clayRobot, obsidianRobot, geodeRobot).maxOf { it.cost.ore }

            while (queue.isNotEmpty()) {
                val state = queue.removeFirst()

                val materials = state.materials
                val robots = state.robotCounts
                val timeRemaining = state.timeRemaining

                best = max(best, materials.geode)
                if (timeRemaining == 0) {
                    if (materials.geode == 49) {
                        var test = ""
                    }
                    continue
                }


                var t = timeRemaining


                // optimalizace ... predictions
                val matOre = t * maxCostOre - robots.ore * (t-1)
                val matClay = t * obsidianRobot.cost.clay - robots.clay * (t-1)
                val matObsidian = t * geodeRobot.cost.obsidian - robots.obsidian * (t-1)
                if (materials.ore >= matOre) materials.ore = matOre
                if (materials.clay >= matClay) materials.clay = matClay
                if (materials.obsidian >= matObsidian) materials.obsidian = matObsidian

                if (visited.count() % 1000_000 == 0) {
                    println("${visited.count()}, ${materials.ore}, ${0}")
                }

                var newState = BlueprintState(
                    Materials(materials.ore,materials.clay,materials.obsidian,materials.geode),
                    RobotCounts(robots.ore, robots.clay, robots.obsidian, robots.geode),
                    t
                )

                if (visited.contains(newState.toString())) {
                    continue
                } else {
                    visited.add(newState.toString())
                }




                var newMaterialsTemplate = Materials(materials.ore+robots.ore,materials.clay+robots.clay,materials.obsidian+robots.obsidian,materials.geode+robots.geode)
                var newRobotCounts = RobotCounts(robots.ore, robots.clay, robots.obsidian, robots.geode)
                queue.push(BlueprintState(newMaterialsTemplate,newRobotCounts, t-1))
                var newMaterials = Materials(newMaterialsTemplate)
                if (materials.ore >= oreRobot.cost.ore) {
                    newMaterials = newMaterialsTemplate.getTranslated(-oreRobot.cost.ore,0,0,0)
                    newRobotCounts = RobotCounts(robots.ore+1,robots.clay,robots.obsidian,robots.geode)
                    queue.push(BlueprintState(newMaterials,newRobotCounts, t-1))
                }
                if (materials.ore >= clayRobot.cost.ore) {
                    newMaterials = newMaterialsTemplate.getTranslated(-clayRobot.cost.ore,0,0,0)
                    newRobotCounts = RobotCounts(robots.ore,robots.clay+1,robots.obsidian,robots.geode)
                    queue.push(BlueprintState(newMaterials,newRobotCounts, t-1))
                }
                if (materials.ore >= obsidianRobot.cost.ore && materials.clay >= obsidianRobot.cost.clay) {
                    newMaterials = newMaterialsTemplate.getTranslated(-obsidianRobot.cost.ore,-obsidianRobot.cost.clay,0,0)
                    newRobotCounts = RobotCounts(robots.ore,robots.clay,robots.obsidian+1,robots.geode)
                    queue.push(BlueprintState(newMaterials,newRobotCounts, t-1))
                }
                if (materials.ore >= geodeRobot.cost.ore && materials.obsidian >= geodeRobot.cost.obsidian) {
                    newMaterials = newMaterialsTemplate.getTranslated(-geodeRobot.cost.ore,0,-geodeRobot.cost.obsidian,0)
                    newRobotCounts = RobotCounts(robots.ore,robots.clay,robots.obsidian,robots.geode+1)
                    queue.push(BlueprintState(newMaterials,newRobotCounts, t-1))
                }

            }


            return best
        }


    }


    open class Materials(var ore: Int = 0, var clay: Int = 0, var obsidian: Int = 0, var geode: Int = 0) {
        fun getTranslated(ore: Int, clay: Int, obsidian: Int, geode: Int): Materials {
            return Materials(
                this.ore + ore,
                this.clay + clay,
                this.obsidian + obsidian,
                this.geode + geode,
            )
        }

        constructor(materials: Materials) : this (materials.ore, materials.clay, materials.obsidian, materials.ore)
    }
    class RobotCounts( var ore: Int = 0, var clay: Int = 0, var obsidian: Int = 0, var geode: Int = 0) {
        constructor(robot: RobotCounts) : this (robot.ore, robot.clay, robot.obsidian, robot.ore)
    }

    class BlueprintState(val materials: Materials, val robotCounts: RobotCounts, val timeRemaining: Int) {
        override fun toString(): String {
            return "${materials.ore},${materials.clay},${materials.obsidian},${materials.geode};${robotCounts.ore},${robotCounts.clay},${robotCounts.obsidian},${robotCounts.geode};${timeRemaining}"
        }
    }


    open class Robot(val cost: Materials = Materials(), var count: Int = 0) {
        constructor(robot: Robot) : this (Materials(robot.cost), robot.count)
    }

    class OreRobot(cost: Materials = Materials(), count: Int = 0) : Robot(cost, count) {
        constructor(robot: OreRobot) : this (Materials(robot.cost), robot.count)
        constructor(robot: Robot) : this (Materials(robot.cost), robot.count)
    }
    class ClayRobot(cost: Materials = Materials(), count: Int = 0) : Robot(cost, count) {
        constructor(robot: ClayRobot) : this (Materials(robot.cost), robot.count)
        constructor(robot: Robot) : this (Materials(robot.cost), robot.count)
    }
    class ObsidianRobot(cost: Materials = Materials(), count: Int = 0) : Robot(cost, count) {
        constructor(robot: ObsidianRobot) : this (Materials(robot.cost), robot.count)
        constructor(robot: Robot) : this (Materials(robot.cost), robot.count)
    }
    class GeodeRobot(cost: Materials = Materials(), count: Int = 0) : Robot(cost, count) {
        constructor(robot: GeodeRobot) : this (Materials(robot.cost), robot.count)
        constructor(robot: Robot) : this (Materials(robot.cost), robot.count)
    }
}

