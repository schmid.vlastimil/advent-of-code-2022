package services.daysOfCode

import java.awt.Point
import java.rmi.UnexpectedException
import kotlin.math.abs

class DayOfCode17: ADayOfCode() {
    override var dayNumber = 17
    override fun makePartOne(): String {
        var answer = simulate(2022)
        return answer.toString()
        return ""
    }


    override fun makePartTwo(): String {


        // just use calculator lol

        return 1_553_982_300_884.toString()
    }

    fun simulate(numberOfRocks: Int): Int {
        val inputData = getInputData(2)

        val map = MutableList(0) { MutableList(7) { '.' } }


        val queue = mutableListOf("xLine", "plus", "mirrorL", "yLine", "square")

        var windIndex = 0
        for (index in 0 until numberOfRocks) {
            val currentShape = queue.removeFirst()
            spawnShape(map, currentShape)

            while (true) {
                val windDirection = when (inputData[windIndex]) {
                    '<' ->  { "left" }
                    '>' ->  { "right" }
                    else -> { "error" }
                }
                if (windIndex == inputData.count()-1) {
                    windIndex = 0
                } else {
                    windIndex++
                }
                simulateDirectionStep(map, windDirection)
                if (!simulateDirectionStep(map, "down")) {
                    break
                }
            }

            convertShapesToRocks(map)
            queue.add(currentShape)


        }

        removeEmptyLines(map)

        return map.count()
    }

    fun render(map: MutableList<MutableList<Char>>) {
        println("========================")
        map.reverse()
        map.forEachIndexed { y, row ->
            val yNumberLine = abs( y-map.count())
            print(yNumberLine.toString().padEnd(3, ' '))
            row.forEachIndexed { x, col ->
                print(col)
            }
            println()
        }
        map.reverse()
    }

    fun removeEmptyLines(map: MutableList<MutableList<Char>>) {
        val indexesToRemove = mutableListOf<Int>()
        map.forEachIndexed { y, row ->
            if (!row.contains('#')) {
                indexesToRemove.add(y)
            }
        }
        indexesToRemove.sortDescending()
        indexesToRemove.forEach {
            map.removeAt(it)
        }
    }

    var beforeCount = 0
    var differenceOfCount = MutableList(0) {0}
    fun spawnShape(map: MutableList<MutableList<Char>>, shapeName: String) {
        // spawn empty lines based on rocks
        removeEmptyLines(map)
//        print("${ (map.count()) - beforeCount},")
        differenceOfCount.add((map.count()) - beforeCount)
        beforeCount = map.count()
        repeat(3) {
            map.add(MutableList(7) { '.' })
        }
        when (shapeName) {
            "xLine" -> {
                map.add("..@@@@.".toMutableList())
            }
            "yLine" -> {
                map.add("..@....".toMutableList())
                map.add("..@....".toMutableList())
                map.add("..@....".toMutableList())
                map.add("..@....".toMutableList())
            }
            "mirrorL" -> {
                map.add("..@@@..".toMutableList())
                map.add("....@..".toMutableList())
                map.add("....@..".toMutableList())
            }
            "square" -> {
                map.add("..@@...".toMutableList())
                map.add("..@@...".toMutableList())
            }
            "plus" -> {
                map.add("...@...".toMutableList())
                map.add("..@@@..".toMutableList())
                map.add("...@...".toMutableList())
            }
            else -> throw UnexpectedException("SHAPE is not here")

        }
    }

    fun convertShapesToRocks(map: MutableList<MutableList<Char>>) {
        map.forEachIndexed { y, chars ->
            chars.forEachIndexed { x, c ->
                if (c == '@')
                    map[y][x] = '#'
            }
        }
    }

    fun copyMap(map: MutableList<MutableList<Char>>): MutableList<MutableList<Char>> {
        val oldMap = MutableList(0) { MutableList(0) {'.'} }
        map.forEachIndexed { rowIndex, row ->
            var stringRow = ""
            row.forEachIndexed { colIndex, col ->
                stringRow += col
            }
            oldMap.add(stringRow.toMutableList())
        }
        return oldMap
    }

    /**
     * @return Boolean true if success simulation else false
     * false when no posible moves
     */
    fun simulateDirectionStep(map: MutableList<MutableList<Char>>, directionTo: String): Boolean {
        val shapePoints = getShapePoints(map)

        when (directionTo) {
            "down" -> {
                shapePoints.translate(0,-1)
            }
            "up" -> {
                shapePoints.translate(0,1)
            }
            "right" -> {
                shapePoints.translate(1,0)
            }
            "left" -> {
                shapePoints.translate(-1,0)
            }
        }

        if (isColision(map, shapePoints)) {
            return false
        }

        map.forEachIndexed { y, chars ->
            chars.forEachIndexed { x, char ->
                if (char == '@') {
                    map[y][x] = '.'
                }
            }
        }

        shapePoints.forEach {
            map[it.y][it.x] = '@'
        }

        return true
    }

    fun getShapePoints(map: MutableList<MutableList<Char>>): List<Point> {
        var points = MutableList(0) { Point() }
        map.forEachIndexed { y, chars ->
            chars.forEachIndexed { x, c ->
                if (c == '@') {
                    points.add(Point(x, y))
                }
            }
        }
        return points.toList()
    }

    fun isColision(map: MutableList<MutableList<Char>>, shapePoints: List<Point>): Boolean {
        shapePoints.forEach { point ->
            if (map.getOrNull(point.y) == null || map[point.y].getOrNull(point.x) == null) {
                return true
            }

            if (map[point.y][point.x] == '#') {
                return true
            }
        }
        return false
    }

    private fun List<Point>.translate(x: Int, y: Int) {
        this.forEach {
            it.translate(x, y)
        }
    }

    fun longestRepeatedSubArray(array: MutableList<Int>): MutableList<Int> {
        val n = array.size
        val LCSRe = Array(n + 1) { IntArray(n + 1) }
        var res = MutableList(0) { 0 } // To store result
        var res_length = 0 // To store length of result

        // building table in bottom-up manner
        var i: Int
        var index = 0
        i = 1
        while (i <= n) {
            for (j in i + 1..n) {
                // (j-i) > LCSRe[i-1][j-1] to remove
                // overlapping
                if (array[i - 1] == array[j - 1]
                    && LCSRe[i - 1][j - 1] < j - i
                ) {
                    LCSRe[i][j] = LCSRe[i - 1][j - 1] + 1

                    // updating maximum length of the
                    // substring and updating the finishing
                    // index of the suffix
                    if (LCSRe[i][j] > res_length) {
                        res_length = LCSRe[i][j]
                        index = Math.max(i, index)
                    }
                } else {
                    LCSRe[i][j] = 0
                }
            }
            i++
        }

        // If we have non-empty result, then insert all
        // characters from first character to last
        // character of String
        if (res_length > 0) {
            i = index - res_length + 1
            while (i <= index) {
                res.add(array[i - 1])
                i++
            }
        }
        return res
    }
}

