package services.daysOfCode

import kotlin.math.pow

class DayOfCode25: ADayOfCode() {
    override var dayNumber = 25

    override fun makePartOne(): String {
        val inputData = getInputData(1)
        val sum = inputData.split("\n").sumOf { snafuToDec(it) }
        return decToSnafu(sum)
    }

    override fun makePartTwo(): String {
        return "START THE BLENDER!!!"
    }

    fun snafuToDec(snafu: String): Long {
        var decNumber = 0L
        snafu.forEachIndexed { index, c ->
            var position = snafu.length - index - 1
            var powPosition = 5.toDouble().pow(position).toLong()
            var oneNumber = 0
            if (c == '0') {
                oneNumber = 0
            } else if (c == '1') {
                oneNumber = 1
            } else if (c == '2') {
                oneNumber = 2
            } else if (c == '=') {
                oneNumber = -2
            } else if (c == '-') {
                oneNumber = -1
            }
            decNumber += (oneNumber * powPosition)
        }
        return decNumber
    }
    fun decToSnafu(dec: Long): String {
        if (dec == 0L)
            return ""

        val x = (dec + 2L) % 5L - 2L
        val y = (dec + 2L) / 5L

        var encoded = ""
        if (x == 0L) {
            encoded = "0"
        } else if (x == 1L) {
            encoded = "1"
        } else if (x == 2L) {
            encoded = "2"
        } else if (x == -2L) {
            encoded = "="
        } else if (x == -1L) {
            encoded = "-"
        }

        return decToSnafu(y) + encoded
    }
}

