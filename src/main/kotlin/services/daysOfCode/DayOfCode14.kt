package services.daysOfCode

import java.awt.Point
import java.lang.Exception
import kotlin.math.max
import kotlin.math.min
 /**
 *                  _.._
 *   _________....-~    ~-.______
 *~~~                            ~~~~-----...___________..--------
 *                                           |   |     |
 *                                           | |   |  ||
 *                                           |  |  |   |
 *                                           |'. .' .`.|
 *___________________________________________|0oOO0oO0o|____________
 * -          -         -       -      -    / '  '. ` ` \    -    -
 *      --                  --       --   /    '  . `   ` \    --
 *---            ---          ---       /  '                \ ---
 *     ----               ----        /       ' ' .    ` `    \  ----
 *-----         -----         ----- /   '   '        `      `   \
 *     .-~~-.          ------     /          '    . `     `    `  \
 *    (_^..^_)-------           /  '    '      '      `
 *Lester||||AMC       --------/     '     '   '
 */
class DayOfCode14: ADayOfCode() {
    override var dayNumber = 14

    override fun makePartOne(): String {
        val inputData = getInputData(1)
        val caveMap = MutableList(1000) { MutableList(1000) { '.' } }

        drawBasicRocks(caveMap, inputData)

        val sandSource = Point(500,0)
        caveMap[sandSource.x][sandSource.y] = '+'

        val answer = simulateSand(caveMap, sandSource)

//        render(caveMap)

        return answer.toString()
    }

    override fun makePartTwo(): String {
        val inputData = getInputData(1)

        val caveMap = MutableList(1000) { MutableList(1000) { '.' } }

        drawBasicRocks(caveMap, inputData)

        var maxY = 0

        inputData.split("\n").forEach { row ->
            row.split(" -> ").forEach {
                val yCoor = it.split(',')[1].toInt()
                if (maxY < yCoor) {
                    maxY = yCoor
                }
            }
        }

        drawRockLine(caveMap, Point(0, maxY+2), Point(999, maxY+2))

        val sandSource = Point(500,0)
        caveMap[sandSource.x][sandSource.y] = '+'

        val answer = simulateSand(caveMap, sandSource) + 1

//        render(caveMap)

        return answer.toString()
    }

    fun simulateSand(map: MutableList<MutableList<Char>>, sandSource: Point): Int {
        var numberOfSand = 0
        while (true) {
            try {
                dropSand(map, sandSource)
            } catch (e: Exception) {
                break
            }
            if (map[sandSource.x][sandSource.y] == 'o') {
                break
            }

            numberOfSand++
        }
        return numberOfSand
    }

    fun drawBasicRocks(map: MutableList<MutableList<Char>>, inputData: String) {
        inputData.split("\n").forEach { rockLine ->
            val firstCoord = rockLine.split(" -> ").first().split(",")
            var pointFrom = Point(firstCoord[0].toInt(), firstCoord[1].toInt())
            rockLine.split(" -> ").forEachIndexed { index, stringCoordinates ->
                val coord = stringCoordinates.split(",")
                val currentPoint = Point(coord[0].toInt(), coord[1].toInt())
                if (index == 0) {
                    pointFrom = currentPoint
                }  else {
                    val pointTo = currentPoint
                    drawRockLine(map, pointFrom, pointTo)
                    pointFrom = pointTo
                }
            }
        }
    }

    fun isSolidOnPoint(map: MutableList<MutableList<Char>>, check: Point): Boolean {
        return !(map[check.x][check.y] != '#' &&
                map[check.x][check.y] != 'o')
    }

    fun dropSand(map: MutableList<MutableList<Char>>, dropPoint: Point) {
        //spawn
        val sandP = Point(dropPoint)
        while (true) {

            if (!isSolidOnPoint(map, Point(sandP.x, sandP.y + 1))) {
                // is floor
                // DOWN
                sandP.translate(0,1)
            } else if (!isSolidOnPoint(map, Point(sandP.x - 1, sandP.y + 1))) {
                // DIAGONAL LEFT
                sandP.translate(-1,1)
            } else if (!isSolidOnPoint(map, Point(sandP.x + 1, sandP.y + 1))) {
                // DIAGONAL RIGHT
                sandP.translate(+1,1)
            } else {
                break
            }
        }
        map[sandP.x][sandP.y] = 'o'
    }

    fun drawRockLine(map: MutableList<MutableList<Char>>, from: Point, to: Point) {
        val maxX = max(from.x, to.x)
        val minX = min(from.x, to.x)
        val maxY = max(from.y, to.y)
        val minY = min(from.y, to.y)

        for (x in minX until maxX + 1) {
            for (y in minY until maxY + 1) {
                map[x][y] = '#'
            }
        }

    }

    fun render(map: MutableList<MutableList<Char>>) {
        // Tutorial Map
//        val minX = 450
//        val minY = 0
//        val maxX = 600
//        val maxY = 20
        // live map
        val minX = 330
        val minY = 0
        val maxX = 680
        val maxY = 170

        print("   ")
        for (x in minX .. maxX) {
            if (x % 5 == 0)
                print(x.toString().padEnd(5, ' '))
        }
        println()


        for (y in minY .. maxY) {
            print(y.toString().padEnd(3, ' '))

            for (x in minX .. maxX) {
                var color = ""
                val reset = "\u001b[0m"
                if (map[x][y] == 'o') {
                    color = "\u001b[93m"
                } else if (map[x][y] == '#') {
                    color = "\u001b[90m"
                } else if (map[x][y] == '.') {
                    color = "\u001b[30m"
                }
                print(color + map[x][y] + reset)

            }
            println()
        }
    }
}

