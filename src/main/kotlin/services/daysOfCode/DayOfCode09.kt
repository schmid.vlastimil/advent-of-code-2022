package services.daysOfCode

import java.awt.Point
import java.nio.file.attribute.PosixFileAttributeView
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sign

/**
 * Rope problem visualization:
 *        |/|
 *        |/|
 *        |/|
 *        |/|
 *        |/|
 *        |/|
 *        |/| /¯)
 *        |/|/\/
 *        |/|\/
 *        (¯¯¯)
 *        (¯¯¯)
 *        (¯¯¯)
 *        (¯¯¯)
 *        (¯¯¯)
 *        /¯¯/\
 *       / ,^./\
 *      / /   \/\
 *     / /     \/\
 *    ( (       )/)
 *    | |       |/|
 *    | |       |/|
 *    | |       |/|
 *    ( (       )/)
 *     \ \     / /
 *      \ `---' /
 *       `-----'
 */
class DayOfCode09: ADayOfCode() {
    override var dayNumber = 9

    var ropePositions: MutableList<Point> = mutableListOf()
    var positionsTailAtLeastOnce: HashSet<String> = hashSetOf<String>()

    override fun makePartOne(): String {
        val inputData = getInputData(1)
        positionsTailAtLeastOnce = hashSetOf<String>()
        ropePositions  = mutableListOf()
        for (tailNum in 1 .. 2) {
            ropePositions.add(Point(0,0))
        }


        inputData.split("\n")
            .forEach {
                val split = it.split(" ")
                move(split[0], split[1].toInt())
            }

        return positionsTailAtLeastOnce.count().toString()
    }

    override fun makePartTwo(): String {
        val inputData = getInputData(1)
        positionsTailAtLeastOnce = hashSetOf<String>()
        ropePositions  = mutableListOf()
        for (tailNum in 1 .. 10) {
            ropePositions.add(Point(0,0))
        }


        inputData.split("\n")
            .forEach {
                val split = it.split(" ")
                move(split[0], split[1].toInt())
            }

        return positionsTailAtLeastOnce.count().toString()
    }

    fun move(direction: String, steps: Int) {
        for (step in 0 until  steps) {
            step(direction)
        }
    }

    fun step(direction: String) {
        ropePositions.forEachIndexed { index, ropePoint ->
            if (index == 0) {
                // Only head
                when (direction) {
                    "R" -> {
                        ropePoint.translate(0, 1)
                    }
                    "L" -> {
                        ropePoint.translate(0, -1)
                    }
                    "D" -> {
                        ropePoint.translate(-1, 0)
                    }
                    "U" -> {
                        ropePoint.translate(1, 0)
                    }
                }
            } else {
                ropePositions.indices.windowed(2) { (head, tail) ->
                    if (!ropePositions[tail].isNeighbourOf(ropePositions[head])) {
                        ropePositions[tail] = ropePositions[tail].movedTo(ropePositions[head])
                    }
                }
            }
        }

        positionsTailAtLeastOnce.add("${ropePositions.last().x},${ropePositions.last().y}")
    }

    fun render() {
        println("OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO")
        val minX = 0
        val minY = 0
        val maxX = 4
        val maxY = 200
//        val minX = -10
//        val minY = -100
//        val maxX = 40
//        val maxY = 100
        for (x in minX .. maxX) {
            for (y in minY .. maxY) {
                var renderedChar = " "

                if (positionsTailAtLeastOnce.contains("${x},${y}"))
                    renderedChar = "#"
                else
                    renderedChar = "."

                ropePositions.forEachIndexed { index, point ->
                    if (point.x == x && point.y == y)
                        renderedChar = (index).toString()
                }

                if (x == ropePositions.first().x && y == ropePositions.first().y)
                    renderedChar = "H"

                print(renderedChar)
            }
            println()
        }
    }
    fun Point.isNeighbourOf(other: Point): Boolean {
        return abs(other.y - y) < 2 && abs(other.x - x) < 2
    }
    fun Point.movedTo(other: Point): Point {
        val moveX = (other.x - x).sign
        val moveY = (other.y - y).sign
        return this + Point(moveX, moveY)
    }
    operator fun Point.plus(other: Point): Point {
        return Point( x + other.x, y + other.y)
    }
}

