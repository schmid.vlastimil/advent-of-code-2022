package services.daysOfCode

import java.awt.Point

//Answer 1: 230
//Done in: 98ms
//Answer 2: 230
//Done in: 213ms
class DayOfCode24: ADayOfCode() {
    override var dayNumber = 24

    var answer1 = 0
    var answer2 = 0

    var start = Point()
    var final = Point()
    var size = Point()
    var maxX = 0
    var maxY = 0

    var directions = listOf(
        Direction.EAST,
        Direction.WEST,
        Direction.SOUTH,
        Direction.NORTH
    )
    var blizMap = mutableMapOf<Char,MutableList<Point>>()

    override fun makePartOne(): String {
        initProgram()

        return getAnswerForAoC("part1").toString()
    }

    override fun makePartTwo(): String {
        initProgram()

        return getAnswerForAoC("part2").toString()
    }

    fun initProgram() {
        blizMap = mutableMapOf<Char,MutableList<Point>>(
            '^' to mutableListOf(),
            'v' to mutableListOf(),
            '<' to mutableListOf(),
            '>' to mutableListOf(),
            '#' to mutableListOf(),
            '.' to mutableListOf()
        )
        val inputData = getInputData(1)
        inputData.split("\n").forEachIndexed { y, s ->
            s.forEachIndexed { x, c ->
                blizMap[c]!!.add(Point(x,y))
            }
        }

        start = blizMap['.']!!.minBy { it.y }
        final = blizMap['.']!!.maxBy { it.y }
        size =  blizMap['#']!!.maxBy { it.x * it.y }
        maxX =  size.x - 1
        maxY =  size.y - 1
        blizMap.remove('.')


        blizMap['#']!!.add(Point(start.x, start.y - 1))
        blizMap['#']!!.add(Point(final.x, final.y + 1))
    }

    fun getAnswerForAoC(part: String = "both"): Int {
        var time = 0
        var specialDirs = directions
        specialDirs = specialDirs.toMutableList()
        specialDirs.add(Direction.NOPE)
        repeat(3) { repeat ->
            var queue = linkedSetOf<Point>()
            queue.add(start)
            while (!queue.contains(final)) {
                time++
                moveBlizzards()
                var impassable = hashSetOf<Point>()
                blizMap.forEach {
                    it.value.forEach { pointF ->
                        impassable.add(pointF)
                    }
                }
                var nextPositions = linkedSetOf<Point>()
                queue.forEach { current ->
                    specialDirs.forEach {  direction ->
                        var delta = direction.getDelta()
                        var new = Point(current)
                        new.translate(delta)
                        if (!impassable.contains(new)) {
                            nextPositions.add(new)
                        }
                    }
                }
                queue = nextPositions
//                println("${time}, ${queue.size}")
                var test = ""
            }
            if (repeat == 0) {
                answer1 = time
                if (part == "part1")
                    return answer1
            }
            val tempFinal = final
            final = start
            start = tempFinal
        }
        answer2 = time
        if (part == "part2")
            return answer2

        return -1
    }

    fun moveBlizzards() {
        directions.forEach { direction ->
            var newBliz = mutableListOf<Point>()
            blizMap[direction.getChar()]!!.forEachIndexed { index, point ->
                point.translate(direction.getDelta())
                if (point.x == 0) {
                    point.x = maxX
                }
                if (point.x > maxX) {
                    point.x = 1
                }
                if (point.y == 0) {
                    point.y = maxY
                }
                if (point.y > maxY) {
                    point.y = 1
                }
                newBliz.add(point)
            }
            blizMap[direction.getChar()] = newBliz
        }
    }

    enum class Direction() {
        NOPE {
            override fun getDelta(): Point { return Point(0,0) }
            override fun getChar(): Char { return 'X' }
        },
        NORTH {
            override fun getDelta(): Point { return Point(0,-1) }
            override fun getChar(): Char { return '^' }
        },
        EAST {
            override fun getDelta(): Point { return Point(1,0) }
            override fun getChar(): Char { return '>' }
        },
        SOUTH {
            override fun getDelta(): Point { return Point(0,1) }
            override fun getChar(): Char { return 'v' }
        },
        WEST {
            override fun getDelta(): Point { return Point(-1,0) }
            override fun getChar(): Char { return '<' }
        };

        abstract fun getDelta(): Point
        abstract fun getChar(): Char
    }

}

private fun Point.translate(delta: Point) {
    translate(delta.x, delta.y)
}

