package services.daysOfCode

import java.math.BigInteger
import java.rmi.UnexpectedException

/**
 *                        .="=.
 *                      _/.-.-.\_     _
 *                     ( ( o o ) )    ))
 *                      |/  "  \|    //
 *      .-------.        \'---'/    //
 *     _|~~ ~~  |_       /`"""`\\  ((
 *   =(_|_______|_)=    / /_,_\ \\  \\
 *     |:::::::::|      \_\\_'__/ \  ))
 *     |:::::::[]|       /`  /`~\  |//
 *     |o=======.|      /   /    \  /
 *jgs  `"""""""""`  ,--`,--'\/\    /
 *                   '-- "--'  '--'
 */
class DayOfCode11: ADayOfCode() {
    override var dayNumber = 11

    override fun makePartOne(): String {
        val inputData = getInputData(1)

        val monkeys = mutableListOf<Monkey>()

        inputData.split("\n\n").forEachIndexed { monkeyIndex, monkeyString ->
            monkeys.add(Monkey.build(monkeyString))
        }

        val leastCommonMultiple = monkeys.map { it.divisionTestNumber }
            .reduce { acc, number -> acc * number }

        repeat(20) {
            monkeys.forEachIndexed { indexCurrentMonkey, monkey ->
                monkey.items.forEachIndexed { itemIndex, itemWorry ->
                    monkey.inspections++

                    val newWorryLevel = computeWorryLevel(itemWorry, monkey.operator, monkey.operationNumber) / 3
                    val reduction = newWorryLevel % leastCommonMultiple

                    var throwToMonkey = 0
                    if (reduction % monkey.divisionTestNumber == 0L) {
                        throwToMonkey = monkey.trueMonkey
                    } else {
                        throwToMonkey = monkey.falseMonkey
                    }
                    monkeys[throwToMonkey].items.add(reduction)
                }
                monkey.items.clear()
            }
        }
        var debug = monkeys.map { it.inspections.toLong() }
        val answer = calculateMonkeyBusiness(monkeys)

        return answer.toString()
    }

    override fun makePartTwo(): String {
        val inputData = getInputData(1)

        val monkeys = mutableListOf<Monkey>()

        inputData.split("\n\n").forEachIndexed { monkeyIndex, monkeyString ->
            monkeys.add(Monkey.build(monkeyString))
        }

        val leastCommonMultiple = monkeys.map { it.divisionTestNumber }
            .reduce { acc, number -> acc * number }

        repeat(10000) {
            monkeys.forEachIndexed { indexCurrentMonkey, monkey ->
                monkey.items.forEachIndexed { itemIndex, itemWorry ->
                    monkey.inspections++

                    val newWorryLevel = computeWorryLevel(itemWorry, monkey.operator, monkey.operationNumber)
                    val reduction = newWorryLevel % leastCommonMultiple

                    var throwToMonkey = 0
                    if (reduction % monkey.divisionTestNumber == 0L) {
                        throwToMonkey = monkey.trueMonkey
                    } else {
                        throwToMonkey = monkey.falseMonkey
                    }
                    monkeys[throwToMonkey].items.add(reduction)
                }
                monkey.items.clear()
            }
        }
        var debug = monkeys.map { it.inspections.toLong() }
        val answer = calculateMonkeyBusiness(monkeys)

        return answer.toString()
    }

    fun calculateMonkeyBusiness(monkeys: List<Monkey>): Long {
        return monkeys.map { it.inspections.toLong() }
            .sortedDescending()
            .take(2)
            .let { inspections -> inspections[0] * inspections[1] }
    }

    private fun computeWorryLevel(worryLevel: Long, operator: String, operationNumber: String): Long {
        return when (operator) {
            "*" -> {
                if (operationNumber == "old")
                    worryLevel * worryLevel
                else
                    worryLevel * operationNumber.toLong()
            }
            "+" -> {
                if (operationNumber == "old")
                    worryLevel + worryLevel
                else
                    worryLevel + operationNumber.toLong()
            }
            else -> throw UnexpectedException("No monkeey can do this")
        }
    }
    data class Monkey(
        val items: MutableList<Long>,
        var inspections: Int = 0,
        val divisionTestNumber: Int,
        val operator: String,
        val operationNumber: String,
        val falseMonkey: Int,
        val trueMonkey: Int
    ) {
        companion object {
            fun build(monkeyString: String): Monkey {
                val lines = monkeyString.split("\n")
                val itemWorries = lines[1].replace(",", "")
                    .replace("  Starting items: ", "")
                    .split(" ")
                    .map { it.toLong() }
                    .toMutableList()
                val operator = lines[2].replace("  Operation: new = old ", "")
                    .split(" ").first()
                val operationNumber = lines[2].replace("  Operation: new = old ", "")
                    .split(" ").last()


                val divisionTestNumber = lines[3].split(" ").last().toInt()
                val trueMonkey = lines[4].split(" ").last().toInt()
                val falseMonkey = lines[5].split(" ").last().toInt()

                return Monkey(
                    items = itemWorries,
                    operator = operator,
                    divisionTestNumber = divisionTestNumber,
                    trueMonkey = trueMonkey,
                    falseMonkey = falseMonkey,
                    operationNumber = operationNumber
                )
            }
        }
    }
}

