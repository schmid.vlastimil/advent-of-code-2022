package services.daysOfCode

class DayOfCode02: ADayOfCode() {
    override var dayNumber = 2

    override fun makePartOne(): String {
        val inputData = getInputData(1);

        var points = 0
        inputData.split("\n").forEachIndexed { index, value ->
            val players = value.split(" ")
            var pasrsedData = parseData(players[0], players[1]);
            // my hand second col
            points += getPointsFromSpecials(pasrsedData[0], pasrsedData[1])[1];
            points += getPointsFromWin(pasrsedData[0], pasrsedData[1])[1];
        }

        return points.toString();
    }

    override fun makePartTwo(): String {
        val inputData = getInputData(1);

        var points = 0
        inputData.split("\n").forEachIndexed { index, value ->
            val players = value.split(" ")
            var pasrsedData = parseData(players[0], players[1]);

            val playersCorrect = chooseCorrectResponse(pasrsedData[0], pasrsedData[1]);
            points += getPointsFromSpecials(playersCorrect[0], playersCorrect[1])[1];
            points += getPointsFromWin(playersCorrect[0], playersCorrect[1])[1];
        }

        return points.toString();
    }

    fun chooseCorrectResponse(p1: Int, response: Int): IntArray {
        var responseArr = IntArray(2)
        responseArr[0] = p1

        if (response == 1) {
            // LOSS
            if (p1 == 1) {
                return intArrayOf(p1, 3)
            } else if (p1 == 2) {
                return intArrayOf(p1, 1)
            } else {
                return intArrayOf(p1, 2)
            }
        } else if (response == 2) {
            // DRAW
            responseArr[1] = p1
        } else {
            // WIN
            if (p1 == 1) {
                return intArrayOf(p1, 2)
            } else if (p1 == 2) {
                return intArrayOf(p1, 3)
            } else {
                return intArrayOf(p1, 1)
            }
        }
        return responseArr
    }

    fun getPointsFromSpecials(p1: Int, p2: Int): IntArray {
        return intArrayOf(p1, p2)
    }

    fun getPointsFromWin(p1: Int, p2: Int): IntArray {
        val points = IntArray(2)

        if ((p1) % 3 == p2 - 1) {
            // player 2 win
            points[0] = 0
            points[1] = 6
        } else if (p1 == p2) {
            // Draw
            points[0] = 3
            points[1] = 3
        } else {
            // player 1 win
            points[0] = 6
            points[1] = 0
        }

        return points
    }

    fun parseData(p1: String, p2: String): IntArray {
        val gameParsed = IntArray(2)

        if (p1 == "A") {
            gameParsed[0] = 1
        } else if (p1 == "B") {
            gameParsed[0] = 2
        } else if (p1 == "C") {
            gameParsed[0] = 3
        }

        if (p2 == "X") {
            gameParsed[1] = 1
        } else if (p2 == "Y") {
            gameParsed[1] = 2
        } else if (p2 == "Z") {
            gameParsed[1] = 3
        }

        return gameParsed
    }
}

