package services.daysOfCode

import org.json.simple.parser.JSONParser
import java.rmi.UnexpectedException
import kotlin.math.min
import kotlin.math.sign

/**
 **                                 .
 *     .              .   .'.     \   /
 *   \   /      .'. .' '.'   '  -=  o  =-
 * -=  o  =-  .'   '              / | \
 *   / | \                          |
 *     |                            |
 *     |                            |
 *     |                      .=====|
 *     |=====.                |.---.|
 *     |.---.|                ||=o=||
 *     ||=o=||                ||   ||
 *     ||   ||                ||   ||
 *     ||   ||                ||___||
 *     ||___||                |[:::]|
 *jgs  |[:::]|                '-----'
 *     '-----'
 */
class DayOfCode13: ADayOfCode() {
    override var dayNumber = 13

    override fun makePartOne(): String {
        val inputData = getInputData(1)
        val packets = inputData.split("\n\n")
        var answer = 0
        packets.forEachIndexed { index, it ->
            val packetSplit = it.split("\n")
            val packet1 = JSONParser().parse(packetSplit[0])
            val packet2 = JSONParser().parse(packetSplit[1])
            val value = analyze(packet1, packet2)

            if (value is Int && value < 0) {
                answer += 1 + index
            }
        }
        return answer.toString()
    }

    override fun makePartTwo(): String {
        var inputData = getInputData(1)
        inputData += "\n\n[[2]]\n[[6]]"
        var packets = inputData.split("\n").toMutableList()
        packets.removeAll(listOf(""))

        for (i in 0 until (packets.count() - 1)) {
            for (j in 0 until (packets.count() - i - 1)) {
                var packet1 = packets[j]
                var packet2 = packets[j+1]
                val value = analyze(JSONParser().parse(packet1), JSONParser().parse(packet2))
                if (!(value is Int && value < 0)) {
                    // not in order
                    packets[j] = packet2
                    packets[j+1] = packet1
                }
            }
        }

        return ((packets.indexOf("[[2]]") + 1) * (packets.indexOf("[[6]]") + 1)).toString()
    }

    fun analyze(packet1: Any?, packet2: Any?): Int {
        var packet1Parsed = packet1
        var packet2Parsed = packet2
        if (packet1 is Number) {
            packet1Parsed = packet1.toLong()
        }
        if (packet2 is Number) {
            packet2Parsed = packet2.toLong()
        }


        if (packet1Parsed is Long && packet2Parsed is Long) {
            return (packet1Parsed - packet2Parsed).sign
        } else if (packet1Parsed is List<*> && packet2Parsed is List<*>) {
            val minimumCount = min(packet1Parsed.count(), packet2Parsed.count())
            for (index in 0 until  minimumCount) {
                val value = analyze(packet1Parsed[index], packet2Parsed[index])
                if (value != 0) {
                    return value
                }
            }

            return analyze(packet1Parsed.count(), packet2Parsed.count())
        } else if (packet1Parsed is List<*> && packet2Parsed is Long) {
            return analyze(packet1Parsed, listOf(packet2Parsed))
        } else if (packet1Parsed is Long && packet2Parsed is List<*>) {
            return analyze(listOf(packet1Parsed), packet2Parsed)
        } else {
            throw UnexpectedException("Fail")
        }
    }

}